'use strict';

angular.module('myApp.api_reporting_bycountry', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/verify/api/reporting/bycountry', {
    templateUrl: 'api_reporting_bycountry/api_reporting_bycountry.html',
    controller: 'api_reporting_bycountryCtrl'
  });
}])

.controller('api_reporting_bycountryCtrl', [function() {

}]);