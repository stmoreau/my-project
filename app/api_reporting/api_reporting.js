'use strict';

angular.module('myApp.api_reporting', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/verify/api/reporting', {
    templateUrl: 'api_reporting/api_reporting.html',
    controller: 'api_reportingCtrl'
  });
}])

.controller('api_reportingCtrl', [function() {

		$(document).ready(function() {
		    $('#dateRangePicker')
		        .datepicker({
		            format: 'dd/M/yyyy',
		            startDate: '01/01/1980',
		            endDate: '12/30/2040'
		        })
		        .on('changeDate', function(e) {
		            // Revalidate the date field
		            $('#dateRangeForm').formValidation('revalidateField', 'date');
		        });
		});


		$(document).ready(function() {
		    $('#dateRangePicker2')
		        .datepicker({
		            format: 'mm/dd/yyyy',
		            startDate: '01/01/1980',
		            endDate: '12/30/2040'
		        })
		        .on('changeDate', function(e) {
		            // Revalidate the date field
		            $('#dateRangeForm2').formValidation('revalidateField', 'date');
		        });
		});



}]);