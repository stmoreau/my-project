'use strict';

// Declare app level module which depends on views, and components
angular
	.module('myApp', [
	  'ngRoute',
	  'myApp.api',
	  'myApp.api_reporting',
	  'myApp.api_reporting_bycountry'
	  ])
	.config(['$routeProvider', function($routeProvider) {

	  $routeProvider.otherwise({redirectTo: '/verify/api'});
	}]);


