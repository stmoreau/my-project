var dashboard = {
    init: function() {
        var self = this;
        self.getData();
    },
    mainRawData: {},
    colorArr: [],
    getData: function() {

        var self = this;

        d3.json("data/data.json", function(err, data) {
            self.mainRawData = data;
            self.aggregatedData = Enumerable.From(self.mainRawData)
                .GroupBy(
                    function(d) {
                        return d['date'];
                    }, // Key selector
                    null, // Element selector
                    function(key, grouping) { // Result selector
                        return {
                            date: key,
                            successful: grouping.Sum("$.successful"),
                            expired: grouping.Sum("$.expired"),
                            failed: grouping.Sum("$.failed")
                        };
                    })
                .OrderBy("$.date")
                .ToArray();

            self.aggregatedData.forEach(function(d) {
                var total = d['successful'] + d['expired'] + d['failed'];
                d['SuccessfulPerc'] = (d['successful'] / total * 100).toFixed(1);
                d['ExpiredPerc'] = (d['expired'] / total * 100).toFixed(1);
                d['FailedPerc'] = (d['failed'] / total * 100).toFixed(1);
            });

            self.renderDashboard();
        });


    },
    renderDashboard: function() {
        var self = this;
        self.createStackedBarChart();
        self.createPieChart();
    },
    createStackedBarChart: function() {
        var self = this;



        stackGroupBarChart({
            chart: {
                divID: "#chart2_stackedBar", // div id where you want to render chart
                width: $("#chart2_stackedBar").width(), //width of chart
                height: 300 //height of chart
            },
            "seriesData": [ //data provided to chart in different series.
                {
                    name: 'Failed',
                    data: self.aggregatedData.map(function(m) {
                        return m.failed;
                    })

                }, {
                    name: 'Expired',
                    data: self.aggregatedData.map(function(m) {
                        return m.expired;
                    })

                }, {
                    name: 'Successful',
                    data: self.aggregatedData.map(function(m) {
                        return m.successful;
                    })

                }
            ],
            rawData: self.aggregatedData,
            colors: ["#A8A8A8", "#CDDAE9", "#1893D3"], //provide color array for series
            events: {
                mouseover: function(d, i) {

                    self.updatePieChart(i, d['seriesName']);
                },
                mouseout: function(d, i) {

                }
            }

        });
    },
    createPieChart: function(dataIndex) {
        var self = this;

        var data = self.getPieData(dataIndex);

        var width = 300,
            height = 300,
            radius = Math.min(width, height) / 2;

        var color = d3.scale.category20();

        var pie = d3.layout.pie()
            .value(function(d) {
                return d.value;
            })
            .sort(null);

        var arc = d3.svg.arc()
            .innerRadius(radius - 90)
            .outerRadius(radius - 40);

        var outerSVG = d3.select("#chart1_pie").append("svg");

        var svg = outerSVG
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + (width / 2 - 40) + "," + height / 2 + ")");


        var path = svg.datum(data).selectAll("path")
            .data(pie)
            .enter().append("path")
            .attr("fill", function(d, i) {
                return d.data.color;
            })
            .attr("d", arc)
            .each(function(d) { this._current = d; }); // store the initial angles


        var middleText = outerSVG.append("text")
            .text("Successful")
            .attr({
                "text-anchor": "middle",
                "x": width / 2 - 40,
                y: (height / 2)
            })
            .style("font-size", "18px")
            .style("font-weight", "bold")
            .style("font-family", "Averta")
            .style("fill", "#1893D3");

        var middleTextPerc = outerSVG.append("text")
            .text(data[0]['perc'] + "%")
            .attr({
                "text-anchor": "middle",
                "x": width / 2 - 40,
                y: (height / 2),
                dy: "1.2em"
            })
            .style("font-size", "14px")
            .style("font-weight", "bold")
            .style("font-family", "Averta")
            .style("fill", "#1893D3");


        self.updatePieChart =
            function(dataIndexUpdate, seriesName) {

                data = self.getPieData(dataIndexUpdate);

                path = svg.datum(data).selectAll("path")
                    .data(pie)
                    .on("mouseover", function(text){
                        svg.select("text").text("kai game");
                    });

                path.transition().duration(750).attrTween("d", arcTween); // redraw the arcs


                var textData = [];
                if (seriesName == 'Successful') {
                    textData = data[0];

                } else if (seriesName == 'Expired') {
                    textData = data[1];
                } else {
                    textData = data[2];
                }

                middleText.text(seriesName)
                    .style("fill", textData.color);

                middleTextPerc
                    .text(textData['perc'] + "%")
                    .style("fill", textData.color);


            };




        // Store the displayed angles in _current.
        // Then, interpolate from _current to the new angles.
        // During the transition, _current is updated in-place by d3.interpolate.
        function arcTween(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function(t) {
                return arc(i(t));
            };
        }

    },
    getPieData: function(dataIndex) {
        var self = this;
        var pieData = {};
        if (dataIndex != undefined) {
            pieData = self.aggregatedData[dataIndex];
        } else {
            pieData = {
                successful: d3.sum(self.aggregatedData, function(d) {
                    return d.successful;
                }),
                expired: d3.sum(self.aggregatedData, function(d) {
                    return d.expired;
                }),
                failed: d3.sum(self.aggregatedData, function(d) {
                    return d.failed;
                })
            };
        }
        var total = (pieData.successful + pieData.expired + pieData.failed);
        return [{
            "label": "Successful",
            "value": pieData.successful,
            "color": "#1893D3",
            perc: ((pieData.successful / total) * 100).toFixed(1)
        }, {
            "label": "Expired",
            "value": pieData.expired,
            "color": "#CDDAE9",
            perc: ((pieData.expired / total) * 100).toFixed(1)
        }, {
            "label": "Failed",
            "value": pieData.failed,
            "color": "#A8A8A8",
            perc: ((pieData.failed / total) * 100).toFixed(1)
        }];
    }


};


$(function() {
    dashboard.init();
});