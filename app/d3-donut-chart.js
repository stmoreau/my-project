function donut(){  
  // Default settings
  var data = {};
  // var showTitle = true;
  var width = 200,
      height = 200,
      radius = Math.min(width, height) / 2;

  var color = d3.scale.ordinal()
  .range(["#083852", "#0c547b", "#148dcd", "#a1d1eb", "#dddddd"]); 

  var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d.value; });

  var svg, g, arc; 


  var object = {};

  // Method for render/refresh graph
  object.render = function(){
    if(!svg){
      arc = d3.svg.arc()
      .outerRadius(radius)
      .innerRadius(radius - (radius/2));

      svg = d3.select('#chart').append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

      g = svg.selectAll(".arc")
        .data(pie(d3.entries(data)))
      .enter().append("g")
      .attr("class", "arc");

      g.append("path")

        .attr("d", arc)
        .style("fill", function(d) { return color(d.data.key); });
      g.append("text")
          .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
          .attr("dy", ".35em")
          .style("text-anchor", "middle");

      svg.append("text")
          .datum(data)
          .attr("x", 0 )
          .attr("y", 6 - radius/10 )
          .attr("class", "text-tooltip1")
          .style("text-anchor", "middle")
          .attr("font-weight", "bold")
          .style("font-size", radius/6+"px")
          .text(function(d){
          return "Verified";
          });

      svg.append("text")
          .datum(data)
          .attr("x", 0 )
          .attr("y", 6 + radius/10 )
          .attr("class", "text-tooltip2")
          .style("text-anchor", "middle")
          .attr("font-weight", "bold")
          .style("font-size", radius/6+"px")
          .text(function(d){
          return data.Verified;
          });


      g.on("mouseover", function(obj){
        console.log(obj);
        svg.select("text.text-tooltip1")
        .attr("fill", function(d) { return color(obj.data.key); })
        .text(function(d){
          return obj.data.key;
        });
        svg.select("text.text-tooltip2")
        .attr("fill", function(d) { return color(obj.data.key); })
        .text(function(d){
          return obj.data.value;
        });
      });




    }  
    return object;
  };

  // Getter and setter methods
  object.data = function(value){
    if (!arguments.length) return data;
    data = value;
    return object;
  };



  return object;
}



var getData = function(){
  var data = {"Verified": 10, "Pending": 20, "Expired": 30, "Failed": 40, "Blacklisted": 5};
  return data;
};




