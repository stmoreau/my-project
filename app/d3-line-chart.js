(function lineChart() {
    var data = lineData(),
        w = 945,
        h = 200,
        margin = 24,
        min = 0,
        max = d3.max(data, function(d) { return d.value }),
        cr = 3,
        xScale = d3.time.scale().range([0, w - margin * 2]).domain([data[0].date, data[data.length-1].date]),
        yScale = d3.scale.linear().range([h - margin * 2, 0]).domain([min, max]),
        xAxis = d3.svg.axis().scale(xScale).tickSize(h - margin * 2).tickPadding(12).ticks(10),
        yAxis = d3.svg.axis().scale(yScale).tickSize(-w + margin * 2).tickPadding(6).orient('left');

    // Chart Area
    var chart = d3.select('#line-chart')
                .append('svg')
                    .attr('width', w)
                    .attr('height', h)
                .append('g')
                    .attr('transform', 'translate(' + margin + ',' + margin + ')');

    // The Axes
    var yAxisg = chart.append('g').classed("yTick", true).call(yAxis);
    var xAxisg = chart.append('g').classed("xTick", true).call(xAxis);
    chart.selectAll('.yTick text')
        .style('fill', 'transparent')
        .style('font-size', '12px');
    chart.selectAll('.xTick text')
        .style('fill', '#555')
        .style('font-size', '12px');
    chart.selectAll('.xTick path')
        .style('fill', 'none')
        .style('stroke', 'transparent')
        .style('stroke-width', .5);
    chart.selectAll('.yTick path')
        .style('fill', 'none')
        .style('stroke', 'transparent')
        .style('stroke-width', .5);
    
    // The Line
    var line = chart.append('g')
                .selectAll('.line')
                .data(data)
                .enter()
                .append('line')
                    .classed('line', true)
                    .style('opacity', 1e-6)
                    .style('fill', 'none')
                    .style('stroke', '#148dcd')
                    .style('stroke-width', 2.4)
                    .attr('x1', function(d, i) { return (i > 0) ? xScale(data[i-1].date) : xScale(d.date); })
                    .attr('y1', function() { return yScale(0); })
                    .attr('x2', function(d) { return xScale(d.date); })
                    .attr('y2', function() { return yScale(0); })
                .transition().duration(250).ease("in-out")
                    .style('opacity', 1)
                    .attr('x1', function(d, i) { return (i > 0) ? xScale(data[i-1].date) : xScale(d.date); })
                    .attr('y1', function(d, i) { return (i > 0) ? yScale(data[i-1].value) : yScale(d.value); })
                    .attr('x2', function(d) { return xScale(d.date); })
                    .attr('y2', function(d) { return yScale(d.value); });

    // The Data Points
    var points = chart.append('g')
                    .selectAll('.data-point')
                    .data(data)
                    .enter()
                    .append('circle')
                        .classed('data-point', true)
                        .style('opacity', 1e-6)
                        .style('fill', "#ffffff")
                        .style('stroke', '#148dcd')
                        .attr('cx', function(d) { return xScale(d.date); })
                        .attr('cy', function(d) { return yScale(d.value); })
                        .attr('r', function() { return cr; })
                        .on('mouseover', function() { d3.select(this).transition().duration(500).ease("elastic").attr('r', cr * 1.5) })
                        .on('mouseout', function() { d3.select(this).transition().duration(500).ease("in-out").attr('r', cr) })
                        .on('click', function(d, i) { console.log(d, i) })
                        .transition().delay(250).duration(500).ease("in-out")
                        .style('opacity', 1)
                        .attr('stroke-width', 2);

    
}());
function lineData() {
    var d = [],
        r = Math.max(Math.round(Math.random()*30), 3);
    while (--r) {
        var date = new Date();
        date.setDate(date.getDate() - r);
        date.setHours(0,0,0,0);
        d.push({'value':Math.round(Math.random()*24), 'date': date});
    }



    return d;
}


