
function VerifyQuality() {
}
(function($) {
	$("body").ready(function() {
		var plot1;

		$("#select_type").change(function(){
			if ($("#start_input").val()!='' && $("#end_input").val()!='')
				VerifyQuality.reloadView($("#select_type").val(),$("#start_input").val(),$("#end_input").val(),$("#net_input").val());
		});
		$("#start_input").change(function(){
			if ($("#start_input").val()!='' && $("#end_input").val()!='')
				VerifyQuality.reloadView($("#select_type").val(),$("#start_input").val(),$("#end_input").val(),$("#net_input").val());
		});
		$("#end_input").change(function(){
			if ($("#start_input").val()!='' && $("#end_input").val()!='')
				VerifyQuality.reloadView($("#select_type").val(),$("#start_input").val(),$("#end_input").val(),$("#net_input").val());
		});
		$("#net_input").change(function(){
			if ($("#start_input").val()!='' && $("#end_input").val()!='')
				VerifyQuality.reloadView($("#select_type").val(),$("#start_input").val(),$("#end_input").val(),$("#net_input").val());
		});

		VerifyQuality.loadChart($("#select_type").val(),$("#start_input").val(),$("#end_input").val(),null);

		//Cross-Browser solution Form Input Date Type
		function inputDateFix() {
			webshims.setOptions('waitReady', false);
			webshim.setOptions("forms-ext", {
				replaceUI: "auto",
				types: "date",
				date: {
					startView: 2,
					openOnMouseFocus: true,
					classes: "show-week"
				},
				number: {
					calculateWidth: false
				},
				range: {
					classes: "show-activevaluetooltip"
				}
			});
			webshims.polyfill('forms forms-ext');
			$.webshims.formcfg = {
					en: {
						dFormat: '-',
						dateSigns: '-',
						patterns: {
							d: "dd-mm-yy"
						}
					}
			};
		}
		inputDateFix();
	});

	VerifyQuality.reloadView = function (type,start,end,net) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		view.getView('/widget/verify_quality/view?t='+type+'&d1='+start+'&d2='+end+'&n='+net, function(data) {
			$("#reports_wait_container").hide();
			$("#reports_view_container").html(data);
		});
		view.getView('/widget/verify_quality/chart?t='+type+'&d1='+start+'&d2='+end+'&n='+net, function(data){
			$("#reports_chart_container").html(data);
			VerifyQuality.loadChart(type,start,end,net);
		});
	};

	VerifyQuality.loadChart = function(type,start,end,net) {
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined') {
				plot1.replot( { resetAxes: true } );
			}
		});
		admin.getVerifyQualityReport(type,start,end,net,function(chart) {
			if (chart!=null && chart!='') {
				plot1 = $.jqplot('chartdiv', chart, {
					series:[
					        {color: "#35b30e"},
					        {color: "#0280A4"},
					        {color: "#EB6F20"},
					        {color: "#7E4BAB"}
					        ],
					        axesDefaults: {
					        	useSeriesColor: true,
					        	tickOptions: {
					        		showMark: false
					        	}
					        },
					        axes: {
					        	xaxis: {
					        		renderer:$.jqplot.DateAxisRenderer,
					        		tickRenderer:$.jqplot.CanvasAxisTickRenderer,
					        		tickOptions: {
					        			angle: -90,
					        			fontFamily: "Arial",
					        			fontSize: "8pt",
					        			formatString: "%d %b %Y",
					        			tickInterval: "1 day"
					        		}
					        	},
					        	yaxis: {
					        		min: 0,
					        		tickOptions:{formatString:'%d'}
					        	}
					        },
					        seriesDefaults: {
					        	color: "#35b30e",
					        	shadow: false,
					        	markerOptions: {
					        		shadow: false
					        	}
					        },
					        grid: {
					        	background: "#ffffff",
					        	borderWidth: 1,
					        	shadow: false
					        },
					        highlighter: {
					        	show: true,
					        	sizeAdjust: 7,
					        	showTooltip: true,
					        	tooltipLocation: "n",
					        	fadeTooltip: true,
					        	tooltipFadeSpeed: "fast",
					        	tooltipOffset: 3,
					        	tooltipAxes: "y",
					        	useAxesFormatters: true
					        }
				});
			};
		});
	};

})(jQuery);

