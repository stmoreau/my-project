// jshint devel:true
'use strict';
(function ($, window, document) {
	$(document).ready(function () {
		$('nav-second-level').collapse({
			'toggle': true,
			'parent': '#nav-aside'
		});
		var $deviceWindow = $(window);
		var $deviceWindowWidth = $(window).width();
		var $this = $(this);
		var $body = $("body");
		var $closePanel = $('.closebox');
		var $hpanel = $('div.hpanel');
        var $icon = $(this).find('i:first');
		var $panelBody = $hpanel.find('div.panel-body');
		var $panelfooter = $hpanel.find('div.panel-footer');
		var $hideShowElem = $('.showhide');
		var $contentWidth = $('#dashboard').width();

		// Activate Table Sorting
		function tableSorter() {
			$("table.table").tablesorter();
		}
		tableSorter();

	    // Toggle Show Details on Account Page
	    function toggleShowDetails() {
	    	var $getLink = $("table.verifypin a[role='button']");
    		var textValShow = 'Show details';
	    	var textValClose = 'Hide details';
    		$getLink.click(function(event) {
    			/* Act on the event */
    			event.preventDefault();
    			var $getVal = $(this).text();
    			if ($getVal == textValShow) {
	    			$(this).text(textValClose);
	    		}else {
	    			$(this).text(textValShow);
	    		}
    		});
	    }
	    toggleShowDetails();

	    // Toggle Bootstrap PopOvers and Tooltips
	    function triggerOverlays() {
	    	$('[data-toggle="tooltip"]').tooltip();
	    	$('[data-toggle="popover"]').popover({html:true});

			$(document).on("click mousedown", '[data-toggle="popover"]', function() {
				$('[data-toggle="popover"]').popover({html:true});
			});

	    	$('[rel="popover"]').popover().click(function() {
	            
	            if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	            	$(this).popover('toggle');
	        		$('[data-trigger="focus"]').attr('data-trigger','click');
	        		$('table .popover').css({
	        			'position': 'absolute',
	        			'display': 'inline-block',
	        			'z-index': '5'
	        		});
	        	}
	        });
	    }
	    triggerOverlays();

	   //Toggle swichBoxes
	    function switchToggle() {
	    	$("[name='save-future']").bootstrapSwitch();
	    }
	    switchToggle();
	    
		// Toggle Panel Menus
		function toggleMenus() {
			$hideShowElem.click(function (event) {
				/* Act on the event */
				event.preventDefault();
				$panelBody.slideToggle(300);
				$panelfooter.slideToggle(200);
				$icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
				$hpanel.toggleClass('').toggleClass('panel-collapse');
				setTimeout(function () {
				    $hpanel.resize();
				    $hpanel.find('[id^=map-]').resize();
				}, 50);
			});
		}
		toggleMenus();

		// Return Back Buttons
		function goHistory() {
			$('button.goback').on('click', function(event) {
				event.preventDefault();
				/* Action on the event */
				history.back(-1);
			});
		}
		goHistory();

		// Close Box Panel Menus
	    function closePanels() {
	    	$closePanel.click(function (event) {
		        event.preventDefault();
		        $hpanel.remove();
		    });
	    }
	    closePanels();

	    
	    // DatePicker
	    function jqueryUIdatePicker() {
	    	//reset type=date inputs to text
			$(document).bind( "mobileinit", function(){
				$.mobile.page.prototype.options.degradeInputs.date = true;
			});
	    }
	    jqueryUIdatePicker();
	    

    	function screenLoads() {
    		$deviceWindow.load(function(){
		    	var $dateInput = $('input.form-control');
		    	var $verifyInput = $('input[type="number"], .input-number');	    	

		    	$dateInput.bind('load', function(e) {
		    		e.preventDefault();
		    		if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	            		$this.attr('type','date').removeClass('ws-date');
	            	}
		    	});		    	
	        }); //End Window.load
    	}
    	screenLoads();


	    // MobileMenu Navigation Mobile Menu
	    function mobileMenu() {
	    	var $getNav = $('#navbar');
	    	var $mobileMenu = $('.mobilemenu');
	    	var $closeIcon = $('<li class="text-right"><a href="#" id="nav-close"><i class="fa fa-close"></i></a></li>');
	    	var $accountBtn = $('.anchor-group');
    		if ($contentWidth <= 992) {
    				$getNav
	    			.clone()
	    			.children('ul')
	    			.appendTo($mobileMenu)
	    			.end();
	    		$mobileMenu.find('ul.nav').addClass('main-menu').removeClass('nav navbar-nav navbar-right pull-right');
	    		$closeIcon.prependTo('.main-menu');
	    		$mobileMenu.find('li.anchor-group').removeClass('anchor-group acc-btn').find('a.dropdown-toggle').remove();	    		
	    		$mobileMenu.find('.dropdown-menu').removeClass('dropdown-menu');	    		
	    		if ($('.mobilemenu li.dropdown').hasClass('active')) {
	    			$('.mobilemenu li.dropdown.active').toggleClass('open').find('ul').css({
	    				'display': 'block'
	    			});
	    		}
	    	} //End If statement
	    }
	    mobileMenu();
	    
	    $deviceWindow.bind('resize', function (e) {	    	
	    	if(!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {		    	
		    	e.preventDefault();		    	
		    	var $nexmobrowserWidth = $(window).width();
	  				if ($nexmobrowserWidth <= 992) {
	  					//location.reload();
	  					window.location.href = window.location.href;
	  				}
		    	mobileMenu();
	    	}	    	
	    });

	    // Initialising Navigation Mobile Menu
	    function navPushMenu() {
	    	/* Navigation Menu Slider */
	        $('#nav-expander').on('click',function(e){
	      		e.preventDefault();	      		
	      		$('body').toggleClass('nav-expanded');
	      		var $nexmobrowserWidth = $(window).width();
	      		if ($nexmobrowserWidth <=992) {
	      			$('#navbar').hide();
	      		}else if($nexmobrowserWidth >=993){
	      			$('#navbar').show();
	      		}
	      	});
	      	$('#nav-close').on('click',function(e){
	      		e.preventDefault();
	      		$('body').removeClass('nav-expanded');
	      	});

	      	$('.dropdown.active a').click(function() {
	      		/* Act on the event */
	      		var $dropDown = $('.dropdown:not(open)');
	      		if ($dropDown === true) {
	      			$this.removeClass('open');
	      		}
	      	});
	      	
	      	// Initialize navgoco with default options
	        $(".main-menu").navgoco({
	            caret: '<span class="caret"></span>',
	            accordion: true,
	            openClass: 'open',
	            save: true,
	            cookie: {
	                name: 'navgoco',
	                expires: false,
	                path: '/'
	            },
	            slide: {
	                duration: 300,
	                easing: 'swing'
	            }
	        });
	    }
	    navPushMenu();

	    // Activate Pull from Navigation to SideBar
	    function asideNavigation() {
    		var $getNavigation = $('#navbar li.dropdown');
    		var $createList = $('#nav-aside').addClass('nav nav-sidebar');
    		var $createElem = $createList.find('li').addClass('navhead');
    		$getNavigation
    			.has('.active')
    			.find('ul')
				.clone(true)
				.addClass('nav nav-second-level')
				.removeClass('dropdown-menu')
				.appendTo($createList)
				.end()
				.end()
				//.remove();
	    }
	    asideNavigation();

	    // DeActivate table header icon in table with keyword 'manage'
	    function tableSortExclude() {
	    	var $tableSorted = $('table').hasClass('tablesorter');
	    	var textValue = $("table > thead > tr > th:last-child:contains('Manage')");
	    	var $lastValue = $("table > thead > tr > th:last-child");
	    	var emptyValue = $lastValue;	    	

	    	//Deactivating Sorter with keyword Manage
	    	if (textValue.length > 0) {
	    		textValue.css({
	    			'background-image': 'none',
	    			'background-color': '#C4C6C9',
	    			'pointer-events': 'none',
	    			'cursor': 'default'
	    		});
	    	}
	    	//Deactivating Table Header with empty header
			if ((emptyValue.children().length == 0) && emptyValue.text().trim() == "") {
	    		emptyValue.css({
	    			'background-image': 'none',
	    			'background-color': '#C4C6C9',
					'pointer-events': 'none',
					'cursor': 'default'
	    		});
	    	}

	    } //End function
	    tableSortExclude();

	    function formSelectImg() {
	    	var $button = $("#formTopUp button");
	    	var selectOption = $('.dd-option input:hidden');
	    	var selectedOption = $('.dd-select input:hidden');

	    	$('.payment-select').ddslick({
	    		data: [],
		        selectText: "",
		        defaultSelectedIndex: null,
		        truncateDescription: true,
		        imagePosition: "left",
		        showSelectedHTML: true,
		        clickOffToClose: true,		        
		        onSelected: function() {
		        	$('#cardType li input:hidden').each(function(i) {						
		        		// if the value exists {
		        		if ( selectedOption.val() != '') {
		        			var valueFromInput = $('.dd-select input:hidden:eq('+i+')').val();
		        			selectedOption.on('change', function() {
		        				if(selectedOption.val() == selectOption.val()) {		        					
			        				// set the value
			        				$(this).val( valueFromInput );
		        				}	        				
		        			}).trigger('change');
		        			$button.on('click', function() {	    
								$('#formTopUp').attr('action', valueFromInput);
							});
		        		}
		        	});
		        	$(document).trigger('paymentMethodSelected');
		        }
	    	});
    	}
    	formSelectImg();
    	//End function

	}); // End .Ready
}(window.jQuery, window, document));