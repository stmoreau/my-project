function PaymentStep2() {
}

(function($) {

	$("body").ready(function() {
		Menu.displayButtons("#menu_topup");
		Menu.enableSettings($("#_uid").val());
		
		jQuery.validator.addMethod("zip", function(value, element) {
			var pattern = /^[a-zA-Z0-9_ -]+$/i;
			return pattern.test(value);
		},"Please enter a valid postal code");

		$("#payment_form_2").validate({
			errorElement: "p",
			errorPlacement: function(error, element) {
				error.addClass("inline-error");
				error.insertAfter(element);
			},
			rules: {
				companyName: {required: true, maxlength:50},
				companyAddress: {required: true, maxlength:150},
				companyCity: {required: true, maxlength:50},
				companyState: {maxlength:20},
				companyZip: {required: true, maxlength:9, zip:true},
				companyTax: {maxlength:25},
				email: {required:true, email:true, maxlength:50},
				firstName: {required:true,maxlength:50},
				lastName: {required:true,maxlength:50},
				phone: {number:true,maxlength:20}
			},
			invalidHandler: function() {
				$("#form-error").remove();
			}
		});
	});

})(jQuery);