
function Tools() {
}

(function($) {

	$("body").ready(function() {

		Tools.enableCdrSearch();
		Tools.enableCdrsSearch();

		Tools.enableRejectedSearch();
		Tools.enableDebugLogSearch();

		//$('#rej_form').submit();

	});


	Tools.enableDebugLogSearch = function() {
		$("#debug_log_form").validate({
			rules: {
				da_debug_dr_input: {number:true,minlength:4}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-debug-error");
				error.appendTo(element.parent("div"));
			},
			submitHandler: function(form) {
				$("#debug_log_submit_container").hide();
				$("#debug_log_wait_container").show();
				view.getView('/widget/messaging/search/debug_log?da='+$("#da_debug_dr_input").val()+'&d='+$("#date_debug_dr_input").val()+'&h='+$("#hour_debug_dr_input").val()+'&t='+$("#type_debug_dr_input").val(), function(data) {
					if (data!=null && data!='void')
						$("#debug_log_result_container").html(data);
					else {
						$("#debug_log_result_container").html('');
					}
					$("#debug_log_submit_container").show();
					$("#debug_log_wait_container").hide();
				});
				return false;
			}
		});

	};

	Tools.enableCdrSearch = function() {

		$("#cdr_form").validate({
			rules: {
				id_input: {required:true, minlength:8}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				id_input: 'Please enter a message ID.'
			},
			submitHandler: function(form) {
				$("#cdr_submit_container").hide();
				$("#cdr_wait_container").show();
				view.getView('/widget/messaging/search/cdr?id='+$("#id_input").val(), function(data) {
					if (data!=null && data!='void') {
						$("#cdr_result_container").html(data);
						Tools.enableHLRCheck();
					} else {
						$("#cdr_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#cdr_submit_container").show();
					$("#cdr_wait_container").hide();
				});
				return false;
			}
		});

	};

	Tools.enableCdrsSearch = function() {

		$("#cdrs_form").validate({
			rules: {
				date_input: {required:true},
				msisdn_input: {required:true,number:true,minlength:4}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				msisdn_input: 'Please enter a phone number.'
			},
			submitHandler: function(form) {
				$("#cdrs_submit_container").hide();
				$("#cdrs_wait_container").show();
				view.getView('/widget/messaging/search/cdrs?m='+$("#msisdn_input").val()+'&d='+$("#date_input").val(), function(data) {
					if (data!=null && data!='void') {
						$("#cdr_result_container").html(data);
						Tools.enableHLRCheck();
					} else {
						$("#cdr_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#cdrs_submit_container").show();
					$("#cdrs_wait_container").hide();
				});
				return false;
			}
		});

	};

	Tools.enableHLRCheck = function() {
		$('.message-set .toggle').click(function(){
			$(this).toggleClass('open');
			var messageSet = $(this).parent().parent();

			if ( $(this).hasClass('open') ) {
				$('.message-set-detail', messageSet).slideDown();
				$(this).text('Hide detail').removeClass('ss-navigatedown').addClass('ss-navigateup');
			} else {
				$('.message-set-detail', messageSet).slideUp();
				$(this).text('Show detail').removeClass('ss-navigateup').addClass('ss-navigatedown');
			}
			return false;
		});

		$(window).resize(function(){
			var windowWidth = $(window).outerWidth();
			if ( windowWidth > 930 ) {
				$('.message-set-detail').show();
			} 
		});

		$('.hlr_link').each(function(i,o) {
			$(this).click(function() {
				var da = $(this).attr('rel');
				$("#hlr_submit_container_"+da).hide();
				view.getView('/widget/hlr/'+da, function(data) {
					if (data!=null && data!='void')
						$("#hlr_container_"+da).html(data);
					else {
						$("#hlr_container_"+da).html('');
					}

				});
			});
		});
	};

	Tools.enableRejectedSearch = function() {

		$("#rej_form").validate({
			rules: {
				rej_date_input: {required:true},
				rej_msisdn_input: {number:true,minlength:4}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				rej_msisdn_input: 'Please enter a valid number.'
			},
			submitHandler: function(form) {
				$("#rej_submit_container").hide();
				$("#rej_wait_container").show();
				view.getView('/widget/messaging/search/rejected?m='+$("#rej_msisdn_input").val()+'&d='+$("#rej_date_input").val(), function(data) {
					if (data!=null && data!='void')
						$("#rej_result_container").html(data);
					else {
						$("#rej_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#rej_submit_container").show();
					$("#rej_wait_container").hide();
				});
				return false;
			}
		});

	};

})(jQuery);
