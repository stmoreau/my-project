
function ProfilePhoneNumber() {
}

(function($) {

	$("body").ready(function() {

		$("#profile_phonenumber_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				phoneNumberCountry: {required:true},
				phoneNumber: {required:true, number:true, maxlength:20, regex: /^(?:[0-9] ?){6,15}[0-9]$/}
			},
			messages: {
				phoneNumber: 'Please provide a valid phone number'
			}
		});

		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

	});

})(jQuery);
