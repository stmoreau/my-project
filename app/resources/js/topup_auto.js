
function TopUpAuto() {
}

(function($) {
	
	$("body").ready(function() {
		
		$("#topup_form").submit(function(e) {
			$("#topup_submit_container").hide();
			$("#topup_wait_container").show();
		});
      	
      	$('#btn_previous').click(function(event) {
      		if (console.log) console.log("'Previous' button pressed...");
			event.preventDefault();
      		$('#btn_hiddenFormForPrevious').click();
			return false;
      	});

	
	});	

	
})(jQuery);	
  