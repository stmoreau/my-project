
function ToolsVerify() {
}

(function($) {

	$("body").ready(function() {

		ToolsVerify.enableCdrSearch();
		ToolsVerify.enableRejectedSearch();
		ToolsVerify.enableCdrsSearch();
	});

	ToolsVerify.enableCdrSearch = function() {

		$("#cdr_form").validate({
			rules: {
				id_input: {required:true, minlength:8}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				id_input: 'Please enter a request ID.'
			},
			submitHandler: function(form) {
				$("#cdr_submit_container").hide();
				$("#cdr_wait_container").show();
				view.getView('/widget/search/cdr-verify?id='+$("#id_input").val(), function(data) {
					if (data!=null && data!='void') {
						$("#cdr_result_container").html(data);
					} else {
						$("#cdr_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#cdr_submit_container").show();
					$("#cdr_wait_container").hide();
				});
				return false;
			}
		});

	};


	ToolsVerify.enableCdrsSearch = function() {

		$("#cdrs_form").validate({
			rules: {
				date_input: {required:true},
				msisdn_input: {required:true,number:true,minlength:8}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				msisdn_input: 'Please enter a phone number.'
			},
			submitHandler: function(form) {
				$("#cdrs_submit_container").hide();
				$("#cdrs_wait_container").show();
				view.getView('/widget/search/cdrs-verify?m='+$("#msisdn_input").val()+'&d='+$("#date_input").val(), function(data) {
					if (data!=null && data!='void') {
						$("#cdr_result_container").html(data);
					} else {
						$("#cdr_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#cdrs_submit_container").show();
					$("#cdrs_wait_container").hide();
				});
				return false;
			}
		});

	};


	ToolsVerify.enableRejectedSearch = function() {

		$("#rej_form").validate({
			rules: {
				rej_date_input: {required:true},
				rej_msisdn_input: {number:true,minlength:8}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				rej_msisdn_input: 'Please enter a valid number.'
			},
			submitHandler: function(form) {
				$("#rej_submit_container").hide();
				$("#rej_wait_container").show();
				view.getView('/widget/search/rejected-verify?m='+$("#rej_msisdn_input").val()+'&d='+$("#rej_date_input").val(), function(data) {
					if (data!=null && data!='void')
						$("#rej_result_container").html(data);
					else {
						$("#rej_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
					$("#rej_submit_container").show();
					$("#rej_wait_container").hide();
				});
				return false;
			}
		});

	};

})(jQuery);

