
function EndUserSDK() {
}

(function($) {

	$("body").ready(function() {
		EndUserSDK.enableActions();
		EndUserSDK.enableAppDelete();
		EndUserSDK.showDeleteConfirmationPopup();
	});

	EndUserSDK.enableActions = function() {
		$(".sdk_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				view.getView('/widget/verify/sdk/view?p='+index, function(data) {
					$('#verify_sdk_result_container').html(data);
					EndUserSDK.enableActions();
				});
				return false;
			});
		});

	};

	EndUserSDK.enableAppDelete = function() {
		$(document).on("click", "#sdk_delete_confirm", function(event){
			var appId = $(this).attr('rel');
			admin.removeSdkApplication(appId,function(ret) {
				if (ret==true) {
					window.location.href = "/verify/sdk/your-apps";
				} else alert('Internal processing error!');
			});
		});
	};

	EndUserSDK.showDeleteConfirmationPopup = function() {
		$(document).on("click", ".deleteSDKAppModal", function(event){
			var appId = $(this).attr('rel');

			view.getView('/widget/delete_popup?appId='+appId, function(data) {
				if (data) {
					$('#sdkAppsListDeleteModal').html(data);
					$('#sdkAppsListDeleteModal').modal('show');
				}
			});
			return false;
		});
	}

})(jQuery);

