
function ShortcodesAlert() {
}

(function($) {

	$("body").ready(function() {

		jQuery.validator.addMethod("email_or_phone", function(value, element) {
			return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value) || /[0-9 -()+]+$/i.test(value);
		},"Please enter an email or a phone number.");

		jQuery.validator.addMethod("email_validator", function(value, element) {
			return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
		},"Please enter an email.");

		jQuery.validator.addMethod("phone_validator", function(value, element) {
			return /^(?:[0-9] ?){6,15}[0-9]$/.test(value);
		},"Please enter a phone number.");

		jQuery.validator.addMethod("optInDescription_validator", function(value, element) {
			var optInValue = $("#binfo-contactOptIn").val();
			if (optInValue == 'WEB') {
				return (value.startsWith("https://") || value.startsWith("http://"));
			} else {
				return true;
			}
		},"Please enter an URL.");

		jQuery.validator.addMethod("character_counter", function(value, element) {
			var regex = new RegExp(/\${(.*?)}/);
			var text = value.replace(regex, "");

			if((text.length < 10) || (text.length > 160)){
				return false;
			}
			return true;
		},"Please enter at least 10, at most 160 characters.");

		ShortcodesAlert.enableCampaignAccountAdd();

		ShortcodesAlert.disableAddDeleteTemplates();

		ShortcodesAlert.addMoreTemplates();
		ShortcodesAlert.deleteLastTemplate();
		ShortcodesAlert.enableSorting();
		ShortcodesAlert.enableMessageCounter();

	});

	ShortcodesAlert.enableCampaignAccountAdd = function() {

		$("#campaign_account_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error shortcodes-alerting-error");
				error.appendTo(element.parent("div"));
			},
			rules: {
				"add_campaign_account_param-template": {character_counter:true},
				"add_campaign_account_program-name": {required:true,minlength:10,maxlength:100},
				"add_campaign_account_param-help": {email_or_phone: true,required:true,maxlength:100},
				"add_campaign_account_param-stop": {email_or_phone: true,required:true,maxlength:100},
				"add_campaign_account_tandc":{required:true},
				"binfo-contactEmail": {email_validator:true},
				"binfo-contactPhone": {phone_validator:true},
				"binfo-contactOptInDescription": {optInDescription_validator:true,required:true}
			},
			messages: {
				"add_campaign_account_program-name": {  required:"Program Name is required",
					minlength:"Please enter at least 10 characters for Program Name",
					maxlength:"Please enter no more than 100 characters for Program Name"},
					"add_campaign_account_param-help": {email_or_phone: "Please enter an email or a phone number for Support Info",
						required:"Support Info is required",
						maxlength:"Please enter no more than 100 characters for Support Info"}
			},

			submitHandler: function(form) {
				$("#campaign_account_submit_container").hide();
				$("#campaign_account_wait_container").show();
				var splitList = $('#add_campaign_account_plist').val().split(",");
				var ks = [];
				var vs = [];
				if (splitList!=null && splitList.length>0) {
					var len = splitList.length;
					for (var i = 0; i < len; i++) {
						var key = splitList[i];
						ks[i]=key;
						vs[i]=$('#add_campaign_account_'+key).val();
					}
				}

				$("textarea[id^=add_campaign_account_param-template-]").each(function(){
					var l = ks.length;
					var key = $(this).attr("id").substring(21);
					ks[l] = key;
					vs[l] = $(this).val();
				});

				var businessInfoKeys = [];
				var businessInfoValues = [];
				$("[id^=binfo-]").each(function(){
					var i = businessInfoKeys.length;
					var key = $(this).attr("id").substring(6);
					businessInfoKeys[i] = key;
					businessInfoValues[i] = $(this).val();
				});

				if($('#campaign_account_id').val() == "" || vs[3] == ""){
					$("#campaign_account_result_container").html("<h5 class='data-feedback'><i class='fa fa-info-circle'></i> We are unable to process your request to generate a new shortcode. Please refresh the page and try again. If the problem persists contact support@nexmo.com</h5>");
					$("#campaign_account_submit_container").show();
					$("#campaign_account_wait_container").hide();
					return false;
				}

				admin.userAddShortcodeCampaignAccount(
						$('#campaign_account_id').val(),
						$('#campaign_id').val(),
						$("input:radio[name='campaign_account_template']:checked").val(),
						ks,
						vs,
						businessInfoKeys,
						businessInfoValues,
						false,
						function(data) {
							if (data!=null && data==true)
								window.location.href="../../your-numbers";
							else {
								$("#campaign_account_result_container").html("<h5 class='data-feedback'><i class='fa fa-info-circle'></i> We are unable to process your request to generate a new shortcode. Please refresh the page and try again. If the problem persists contact support@nexmo.com</h5>");
								$("#campaign_account_submit_container").show();
								$("#campaign_account_wait_container").hide();
							}
						});
				return false;
			}
		});

		$("textarea[id^=add_campaign_account_param-template-]").each(function() {
			$(this).rules("add", {
				required:true,
				minlength:10,
				maxlength:160
			});
		});
	};

	ShortcodesAlert.disableAddDeleteTemplates = function() {
		var num = $("div[id^=template]").length;
		if (num > 3)
			$("#add_more_templates").hide();
		else
			$("#add_more_templates").show();
		if (num < 1)
			$("#delete_last_template").hide();
		else
			$("#delete_last_template").show();
	};

	ShortcodesAlert.addMoreTemplates = function() {
		$("#add_more_templates").click(function(){
			var num = $("div[id^=template]").length;
			if(num < 4){
				var newId = "template"+(num+1);
				$("#add_more_templates_container").append("<div id='" + newId + "'></div>");
				var newDiv = "#" + newId; 
				$(newDiv).html($("#param_template").html());
				$(newDiv).find("#template_counter_message").remove();
				$(newDiv).find("textarea").attr("id", "add_campaign_account_param-template-"+(num+1));
				$(newDiv).find(".control-label").text("Message (template-" +(num+1) +  ") ");
				$(newDiv).find("textarea").attr("name", "add_campaign_account_param-template-"+(num+1));
				$(newDiv).find("textarea").attr("value", "");
				$(newDiv).find("textarea").attr("class", "text extraparam form-control form-block");
				var newTextArea = "#add_campaign_account_param-template-"+(num+1);
				$(newTextArea).rules("add", {
					required:true,
					minlength:10,
					maxlength:160
				});
			}
			ShortcodesAlert.disableAddDeleteTemplates();
		});
	};

	ShortcodesAlert.deleteLastTemplate = function() {
		$("#delete_last_template").click(function(){
			var num = $("div[id^=template]").length;
			var templateId = "#template"+num;
			$(templateId).remove();
			ShortcodesAlert.disableAddDeleteTemplates();
		});
	};

	ShortcodesAlert.enableSorting = function() {
		$("div#add_more_templates_container>div").tsort("",{attr:"id"});
	};

	ShortcodesAlert.enableMessageCounter = function(){
		var regex = new RegExp(/\${(.*?)}/);

		$( "#add_campaign_account_param-template" ).keyup(function( event ) {
			var text = $("#add_campaign_account_param-template").val();
			text = text.replace(regex, "");	

			$("#totalLength").html(text.length);

		}).keydown(function( event ) {
			if ( event.which == 13 ) {
				event.preventDefault();
			}
		});
	};

})(jQuery);
