function PaymentEditMethod() {
}

(function($) {

    $("body").ready(function() {
        PaymentEditMethod.customValidator();
        PaymentEditMethod.bindEvents();
        PaymentEditMethod.validateInit();
        PaymentEditMethod.modalDeletePayment();
        PaymentEditMethod.enablePaymentInstrumentHistoryPagination();
        PaymentEditMethod.enablePaymentHistoryPagination();
    });

    PaymentEditMethod.customValidator = function(){
        $.formUtils.addValidator({
                name : 'billingZip_custom',
                validatorFunction : function(value, $el, config, language, $form)
                {
                    if (value === '') {
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-empty']);
                    }else if (value.length > 9){
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-length']);
                    }else if (/^[a-zA-Z0-9_ -]+$/i.test(value) !== true){
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-valid']);
                    }else{
                        return true;
                    }
                }
        });
    };

    PaymentEditMethod.validateInit = function(){
        var rules = {
            form: '#editPaymentMethodForm',
            validate: {
                'billingName': {
                    validation: 'required length',
                    'length': 'max100',
                    'error-msg': 'Please enter your full name',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'companyName': {
                    validation: 'length',
                    'length': 'max50',
                    'error-msg-length': 'Please do not enter more then 50 characters'
                },
                'billingAddress': {
                    validation: 'required length',
                    'length': 'max100',
                    'error-msg': 'Please enter your street address',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'billingAddressLine2': {
                    validation: 'length',
                    'length': 'max100',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'billingCity': {
                    validation: 'required length',
                    'length': 'max50',
                    'error-msg': 'Please enter your city',
                    'error-msg-length': 'Please do not enter more then 50 characters'
                },
                'billingState': {
                    validation: 'length',
                    'length': 'max20',
                    'error-msg-length': 'Please do not enter more then 20 characters'
                },
                'billingCountry': {
                     validation: 'required',
                    'error-msg': 'Please enter your country'
                },
                'billingZip': {
                    validation: 'billingZip_custom',
                   'error-msg-empty': 'Please enter your zip or postcode',
                   'error-msg-valid': 'Please enter a valid zip or postal code',
                   'error-msg-length': 'Please do not enter more then 9 characters'
                },
                'amount': {
                    validation: 'required',
                    'error-msg': 'Please select a payment amount'
                }     
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });
    };

    PaymentEditMethod.bindEvents = function(){
        $('form input').bind('validation', function(evt, isValid) { 
            if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide fa-check-circle').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });
    }; 

    PaymentEditMethod.modalDeletePayment = function(){
        $('#form-delete-payment').submit(function(e){
            e.preventDefault();
            admin.savePaymentInstrument($('#paymentInstrument').val(), 0);
            //$('#'+e.target.id+' .modal-body, #'+e.target.id+' .modal-title, #'+e.target.id+' [type="submit"]').addClass('hide');
            //$('#'+e.target.id+' .modal-success, #'+e.target.id+' .modal-title-success').removeClass('hide');
            //$('.modal-footer [data-dismiss="modal"]').text('Close');
        	$('#backPaymentMethod')[0].click();
        });
        /*
        $('#form-delete-payment').closest('.modal').on('hidden.bs.modal', function (e) {
            $('#'+e.target.id+' .modal-body, #'+e.target.id+' .modal-title, #'+e.target.id+' [type="submit"]').removeClass('hide');
            $('#'+e.target.id+' .modal-success, #'+e.target.id+' .modal-title-success').addClass('hide');
            if($('.modal-footer [data-dismiss="modal"]').text() == 'Close'){
            	$('.modal-footer [data-dismiss="modal"]').text('Cancel');
            	$('#backPaymentMethod')[0].click();
            }
        });
        
        $('#form-delete-payment').closest('.modal').on('shown.bs.modal', function () {
            $(this).find('input[type=text],textarea,select').filter(':visible:first').focus();
        });
        */
    };

    PaymentEditMethod.reloadPaymentInstrumentHistory = function(index, piId) {
        view.getView('/widget/search/topup?p='+index + "&pi="+piId, function(data) {
            $('#payment_edit_section_3').html(data);
            PaymentEditMethod.enablePaymentInstrumentHistoryPagination();
        });
    };

    PaymentEditMethod.enablePaymentInstrumentHistoryPagination = function() {
        $(".topup_pagination_link").each(function(i,o) {
            $(this).click(function() {
               PaymentEditMethod.reloadPaymentInstrumentHistory($(this).attr('rel'), $("#payment_instrument_id").val());
               return false;
            });
        });
    };
    
    PaymentEditMethod.enablePaymentHistoryPagination = function() {
        $(".topup_pagination_link").each(function(i,o) {
            $(this).click(function() {
            	PaymentEditMethod.reloadPaymentHistory($(this).attr('rel'));
                return false;
            });
        });
    };
    
    PaymentEditMethod.reloadPaymentHistory = function(index) {
        view.getView('/widget/search/topup?p='+index, function(data) {
            $('#payment_edit_section_3').html(data);
            PaymentEditMethod.enablePaymentHistoryPagination();
            Payment.enableTopUpInvoice();
        });
    };

})(jQuery);	
