function PaymentNewMethod() {
}

(function($) {

    $("body").ready(function() {
        PaymentNewMethod.customValidator();
        PaymentNewMethod.bindEvents();
        PaymentNewMethod.validateInit();
        PaymentNewMethod.popover();

        if (typeof isPartialPage == 'undefined' || ! isPartialPage) {
        	var merchantId;
            admin.getBraintreeMerchantId(function(ret) { merchantId = ret;});
            admin.getBraintreeEnvironment(function(ret) {
                if(ret=="SANDBOX") BraintreeData.setup(merchantId, 'braintreeData', BraintreeData.environments.sandbox);
                else if(ret == "PRODUCTION") BraintreeData.setup(merchantId, 'braintreeData', BraintreeData.environments.production);
            });
        } else {
        	if (console.log) console.log("isPartialPage ? "+ isPartialPage);
        }

    });

    PaymentNewMethod.customValidator = function(){
        $.formUtils.addValidator({
                name : 'billingZip_custom',
                validatorFunction : function(value, $el, config, language, $form)
                {
                    if (value === '') {
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-empty']);
                    }else if (value.length > 9){
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-length']);
                    }else if (/^[a-zA-Z0-9_ -]+$/i.test(value) !== true){
                        $el.attr('data-validation-error-msg', config.validate.billingZip['error-msg-valid']);
                    }else{
                        return true;
                    }
                }
        });
    };

    PaymentNewMethod.validateInit = function(){
        var rules = {
            form: '#newPaymentMethodForm',
            validate: {
                'billingName': {
                    validation: 'required length',
                    'length': 'max100',
                    'error-msg': 'Please enter your full name',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'companyName': {
                    validation: 'length',
                    'length': 'max50',
                    'error-msg-length': 'Please do not enter more then 50 characters'
                },
                'billingAddress': {
                    validation: 'required length',
                    'length': 'max100',
                    'error-msg': 'Please enter your street address',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'billingAddressLine2': {
                    validation: 'length',
                    'length': 'max100',
                    'error-msg-length': 'Please do not enter more then 100 characters'
                },
                'billingCity': {
                    validation: 'required length',
                    'length': 'max50',
                    'error-msg': 'Please enter your city',
                    'error-msg-length': 'Please do not enter more then 50 characters'
                },
                'billingState': {
                    validation: 'length',
                    'length': 'max20',
                    'error-msg-length': 'Please do not enter more then 20 characters'
                },
                'billingCountry': {
                     validation: 'required',
                    'error-msg': 'Please enter your country'
                },
                'billingZip': {
                     validation: 'billingZip_custom',
                    'error-msg-empty': 'Please enter your zip or postcode',
                    'error-msg-valid': 'Please enter a valid zip or postal code',
                    'error-msg-length': 'Please do not enter more then 9 characters'
                },
                'amount': {
                    validation: 'required',
                    'error-msg': 'Please select a payment amount'
                }     
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });
    };

    PaymentNewMethod.bindEvents = function(){
        $('form input').bind('validation', function(evt, isValid) { 
            if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide fa-check-circle').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });
    }; 

    PaymentNewMethod.popover = function(){
        $('.popover-hint').popover({
             html: true,
             template: '<div class="popover popover-normal"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div></div>'
        });
    };

})(jQuery);	
