function ShortcodeMarketing1() {
}

(function($) {

	$("body").ready(function() {

		jQuery.validator.addMethod("keyword", function(value, element) {
			var pattern = /^[a-zA-Z0-9]+$/i;
			return pattern.test(value);
		},"Keyword must be alphanumeric, no space and/or special characters");

		$("#marketing_form_1").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				keyword: {keyword:true,required:true,maxlength:10}
			}
		});
	});

})(jQuery);