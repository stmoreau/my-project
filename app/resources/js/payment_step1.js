function PaymentStep1() {
}

(function($) {
	$("body").ready(function() {
		
		$('#payment_form_1').get(0).reset();

		Menu.displayButtons("#menu_topup");

		Menu.enableSettings($("#_uid").val());

		$("#payment_form_1").validate({
			errorElement: "p",
			errorPlacement: function(error, element) {
				error.addClass("inline-error");
				error.insertAfter(element);
			},
			rules: {
				requestedBillingCountry: {required:true},
				paymentMethod: {required:true},
				denomination: {required:true}
			}
		});

		$("#requestedBillingCountry").change(function(){
			view.getView('/widget/get_payment_methods?co='+$("#requestedBillingCountry").val()+'&s='+$("#_payment_method").val(), function(data) {
				$("#payment_method_container").html(data);
				$("#payment_method_container").show();
			});
			view.getView('/widget/get_denominations?co='+$("#requestedBillingCountry").val()+'&s='+$("#_denomination").val(), function(data) {
				$("#denomination_container").html(data);
				$("#denomination_container").show();
			});
		});

		
});

})(jQuery);