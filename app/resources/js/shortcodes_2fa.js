
function Shortcodes2FA() {
}

(function($) {

	$("body").ready(function() {
		Shortcodes2FA.enableCampaignAccountAdd();
	});

	Shortcodes2FA.enableCampaignAccountAdd = function() {

		$("input[name='campaign_account_template']").on("change", function () {
			var v = $(this).val();
			$(".rowfield").each(function(i,o) { $(this).hide()});
			$(".rowtpl"+v).each(function(i,o) { $(this).show()});
		});

		$("#campaign_account_form").validate({
			rules: {
				"add_campaign_account_company-name": {required:true,minlength:2,maxlength:15},
				"add_campaign_account_duration": {number:true,min:1,max:60}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			submitHandler: function(form) {
				$("#campaign_account_submit_container").hide();
				$("#campaign_account_wait_container").show();
				var splitList = $('#add_campaign_account_plist').val().split(",");
				var ks = [];
				var vs = [];
				if (splitList!=null && splitList.length>0) {
					var len = splitList.length;
					for (var i = 0; i < len; i++) {
						var key = splitList[i];
						ks[i]=key;
						vs[i]=$('#add_campaign_account_'+key).val();
					}
				}

				if($('#campaign_account_id').val() == ""){
					$("#campaign_account_result_container").html("<h5 class='data-feedback'><i class='fa fa-info-circle'></i> We are unable to process your request to generate a new shortcode. Please refresh the page and try again. If the problem persists contact support@nexmo.com</h5>");
					$("#campaign_account_submit_container").show();
					$("#campaign_account_wait_container").hide();
					return false;
				}

				admin.userAddShortcodeCampaignAccount(
						$('#campaign_account_id').val(),
						$('#campaign_id').val(),
						$("input:radio[name='campaign_account_template']:checked").val(),
						ks,
						vs,
						null,
						null,
						true,
						function(data) {
							if (data!=null && data==true)
								window.location.href="../../your-numbers";
							else 
								$("#campaign_account_result_container").html("<h5 class='data-feedback'><i class='fa fa-info-circle'></i> We are unable to process your request to generate a new shortcode. Please refresh the page and try again. If the problem persists contact support@nexmo.com</h5>");
							$("#campaign_account_submit_container").show();
							$("#campaign_account_wait_container").hide();
						});
				return false;
			}
		});
	};

})(jQuery);