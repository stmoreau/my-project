
function RatiosNetworks() {
}

(function($) {
	
	$("body").ready(function() {

		$("#select_date").change(function(){
			RatiosNetworks.reloadView($("#select_date").val(),$("#select_account").val());
		})				
		
		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				RatiosNetworks.reloadView($("#select_date").val(),$("#select_account").val());
			});				
		}
		
		RatiosNetworks.enableSorting();
		
	});

	RatiosNetworks.reloadView = function (date,account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/ratios/networks/view/'+date+'?a='+account, function(data) {
				$("#reports_wait_container").hide();
				$("#reports_view_container").html(data);
				RatiosNetworks.enableSorting();
			});
		} else {
			view.getView('/widget/ratios/networks/view/'+date, function(data) {
				$("#reports_wait_container").hide();
				$("#reports_view_container").html(data);
				RatiosNetworks.enableSorting();
			});
		}
	}
	

	RatiosNetworks.enableSorting = function () {

		if ( $('#report_table').is('*') ) { 
			$('#network_download_link').attr("href",$('#download_link_prefix').val()+$("#select_date").val());
			$('#network_download_link').show();
		} else $('#network_download_link').hide();

		$('#report_table').tablesorter();
		var pageRow;

	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });

        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);
        });
	}

})(jQuery);
