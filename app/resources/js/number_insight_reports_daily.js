
function NIReportsDaily() {
}

(function($) {

	$("body").ready(function() {

		var plot1;

		NIReportsDaily.loadChart($("#select_type").val(),$("#select_date").val(),null);

		$("#select_type").change(function() {
			$("#reports_view_container").html('');
			$("#chartdiv").html('');
			$('#network_download_link').hide();
			NIReportsDaily.reloadDates($("#select_type").val(),$("#select_account").val());
		});

		$("#select_date").change(function(){
			NIReportsDaily.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		});

		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				NIReportsDaily.reloadDates($("#select_type").val(),$("#select_account").val());
			});
		}

		NIReportsDaily.enableSorting();
	});

	NIReportsDaily.reloadDates = function (type,account) {
		view.getView('/widget/ni/get_dates?t='+type+'&a='+account, function(data) {
			$("#reports_date_container").html(data);
			$("#reports_view_container").html('');
			$('#network_download_link').hide();
			$("#chartdiv").html('');
			//$("#select_date").change(function(){
			NIReportsDaily.reloadView($("#select_type").val(),$("#select_date").val(),account);
			//});
		});
	}

	NIReportsDaily.reloadView = function (type,date,account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		$("#chartdiv").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/ni/networks/view/'+date+'?t='+type+'&a='+account, function(data) {
				$("#reports_view_container").html(data);
				$("#reports_wait_container").hide();
				NIReportsDaily.enableSorting();
				NIReportsDaily.loadChart(type,date,account);

				if ( $('#report_table').is('*') ) {
					$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
					$('#network_download_link').show();
				} else $('#network_download_link').hide();
			});
		} else {
			view.getView('/widget/ni/networks/view/'+date+'?t='+type, function(data) {
				$("#reports_view_container").html(data);
				$("#reports_wait_container").hide();

				NIReportsDaily.enableSorting();
				NIReportsDaily.loadChart(type,date,null);

				if ( $('#report_table').is('*') ) {
					$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
					$('#network_download_link').show();
				} else $('#network_download_link').hide();
			});
		}
	}

	NIReportsDaily.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;

		$("#report_table").bind("sortStart", function () {
			// remove the row with the class nosort
			pageRow = $(this).children("tbody").children("tr.nosort");
			$(this).children("tbody").children("tr.nosort").remove();
		});

		$("#report_table").bind("sortEnd", function () {
			// add the removed row
			$(this).children("tbody").append(pageRow);
		});
	}

	NIReportsDaily.loadChart = function(type,date,account) {
		$('#chartdiv').html('');
		$('#network_download_link').hide();
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined') {
				plot1.replot( { resetAxes: true } );
			}
		});
		admin.getAccountNetworkReport(type,date,account,function(chart) {
			if (chart!=null && chart!='') {
				$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
				$('#network_download_link').show();
				plot1 = $.jqplot('chartdiv', [chart], {
					series:[{renderer:$.jqplot.BarRenderer,
						rendererOptions: {
							barWidth: 5,
							color: "#35b30e",
							shadow: false,
							markerOptions: {
								shadow: false
							}
						}}],
						axesDefaults: {
							tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
							tickOptions: {
								angle: -30
							}
						},
						axes: {
							xaxis: {
								renderer: $.jqplot.CategoryAxisRenderer
							},
							yaxis: {
								min: 0,
								tickOptions:{formatString:'%d'},
								autoscale:true
							}
						},
						grid: {
							gridLineColor: "#e8e8e8",
							background: "#ffffff",
							borderColour: "#e8e8e8",
							borderWidth: 0,
							shadow: false
						},
						highlighter: {
							show: true,
							sizeAdjust: 7,
							showTooltip: true,
							tooltipLocation: "n",
							fadeTooltip: true,
							tooltipFadeSpeed: "fast",
							tooltipOffset: 3,
							tooltipAxes: "y",
							useAxesFormatters: true
						}
				});
			}
		});
	}

})(jQuery);
