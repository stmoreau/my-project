
function ReportsNetwork() {
}

(function($) {

	$("body").ready(function() {

		var plot1;

		ReportsNetwork.loadChart($("#select_type").val(),$("#select_date").val(),null);

		$("#select_type").change(function() {
			$("#reports_view_container").html('');
			$("#chartdiv").html('');
			$('#network_download_link').hide();
			ReportsNetwork.reloadDates($("#select_type").val(),$("#select_account").val());
		});

		$("#select_date").change(function(){
			ReportsNetwork.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		});

		if ( $('#select_account').is('*') ) {
			$("#select_account").change(function(){
				ReportsNetwork.reloadDates($("#select_type").val(),$("#select_account").val());
			});
		}

		ReportsNetwork.enableSorting();
	});

	ReportsNetwork.reloadDates = function (type,account) {
		view.getView('/widget/verify/get_dates?t='+type+'&a='+account, function(data) {
			$("#select_date").html(data);
			$("#reports_view_container").html('');
			$("#chartdiv").html('');
			ReportsNetwork.reloadView($("#select_type").val(),$("#select_date").val(),account);
		});
	}

	ReportsNetwork.reloadView = function (type,date,account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		$("#chartdiv").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/verify/networks/view/'+date+'?t='+type+'&a='+account, function(data) {
				$("#reports_view_container").html(data);
				ReportsNetwork.enableSorting();
				ReportsNetwork.loadChart(type,date,account);
				$("#reports_wait_container").hide();
			});
		} else {
			view.getView('/widget/verify/networks/view/'+date+'?t='+type, function(data) {
				$("#reports_view_container").html(data);
				ReportsNetwork.enableSorting();
				ReportsNetwork.loadChart(type,date,null);
				$("#reports_wait_container").hide();
			});
		}
	}

	ReportsNetwork.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;

		$("#report_table").bind("sortStart", function () {
			// remove the row with the class nosort
			pageRow = $(this).children("tbody").children("tr.nosort");
			$(this).children("tbody").children("tr.nosort").remove();
		});

		$("#report_table").bind("sortEnd", function () {
			// add the removed row
			$(this).children("tbody").append(pageRow);	 
		});
	}

	ReportsNetwork.loadChart = function(type,date,account) {
		$('#chartdiv').html('');
		$('#network_download_link').hide();
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined') {
				plot1.replot( { resetAxes: true } );
			}
		});
		admin.getAccountNetworkReport(type,date,account,function(chart) {
			if (chart!=null && chart!='') {
				$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
				$('#network_download_link').show();
				plot1 = $.jqplot('chartdiv', [chart], {
					series:[{renderer:$.jqplot.BarRenderer,
						rendererOptions: {
							barWidth: 5,
							color: "#35b30e",
							shadow: false,
							markerOptions: {
								shadow: false
							}
						}}],
						axesDefaults: {
							tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
							tickOptions: {
								angle: -30
							}
						},
						axes: {
							xaxis: {
								renderer: $.jqplot.CategoryAxisRenderer
							},
							yaxis: {
								min: 0,
								tickOptions:{formatString:'%d'},
								autoscale:true
							}
						},
						grid: {
							gridLineColor: "#e8e8e8",
							background: "#ffffff",
							borderColour: "#e8e8e8",
							borderWidth: 0,
							shadow: false
						},
						highlighter: {
							show: true,
							sizeAdjust: 7,
							showTooltip: true,
							tooltipLocation: "n",
							fadeTooltip: true,
							tooltipFadeSpeed: "fast",
							tooltipOffset: 3,
							tooltipAxes: "y",
							useAxesFormatters: true
						}
				});
			}
		});
	}

})(jQuery);
