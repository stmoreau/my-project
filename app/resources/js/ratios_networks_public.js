
function RatiosNetworksPublic() {
}

(function($) {

	$("body").ready(function() {

		$("#select_country").change(function(){
			RatiosNetworksPublic.reloadView($("#select_country").val());
		})

		RatiosNetworksPublic.enableSorting();

	});

	RatiosNetworksPublic.reloadView = function (co) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		view.getView('/widget/ratios/networks/public/view/'+co, function(data) {
			$("#reports_wait_container").hide();
			$("#reports_view_container").html(data);
			RatiosNetworksPublic.enableSorting();
		});
	}


	RatiosNetworksPublic.enableSorting = function () {

		$('#report_table').tablesorter();
		var pageRow;

	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });

        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);
        });
	}


})(jQuery);
