(function ($) {

	// keepSquare
	$.fn.keepSquare = function(){
		var elementWidth = this.width();
		this.height(elementWidth);
		return this;
	};

	// mobileMenu
	$.fn.mobileMenu = function(){
		var menu = this.html();
		var topMenu = $('#top_secondary_nav').html();
		$('body').prepend('<div id="mobile_menu"><div class="top_menu">'+topMenu+'</div><div class="main_menu">'+menu+'</div></div>');
		return this;
	};

}(jQuery));

jQuery(document).ready(function($) {

	var windowWidth = $(window).width();

	if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		//Add style to logo for firefox
		$('#logo').addClass('no-spin');
	}

	/****************************************************************
	Build mobile menu
	****************************************************************/
	$('#header_menu_container').mobileMenu();

	/****************************************************************
	Animate the menu stuff
	****************************************************************/
	$('button.menu-toggle').click(function(){

		var windowWidth = $(window).width();
		var rightOffset = windowWidth - 60;

		if( $('button.menu-toggle').hasClass('expanded') ) {

			$('#page').animate({
				right: 0
			}, {
				duration: 120,
				specialEasing: "swing"
			});

			$('button.menu-toggle').removeClass('expanded');

		} else {

			$('#page').animate({
				right: rightOffset
			}, {
				duration: 120,
				specialEasing: "swing"
			});

			$('button.menu-toggle').addClass('expanded');

		}

		return false;

	});

	/****************************************************************
	Scrollto menu navigation offset
	****************************************************************/
	$('.page_secondary_navigation ul > li > a').click(function(){

		//get header height
		var smallHeaderHeight = 92;

		//get id of anchor
		var anchorHref = $(this).attr('href');
		var anchorID = anchorHref.replace( '#', '' );

		//find matching element in body
		var anchorSection = $('body').find(anchorHref).parents('section');

		//scroll the window down to the top of that section
		$('html, body').animate({
			scrollTop: $(anchorSection).offset().top - smallHeaderHeight
		}, 200);

		return false;

	});

});