function Settings() {
}

(function($) {

	$("body").ready(function() {

		Settings.addCustomValidation();
		Settings.initValidation();

		$(document).on("change", "#sysId", function(event){
			Settings.apiKeyreload($("#sysId").val());
		});

	});

	Settings.apiKeyreload = function(account) {
		if (account!=null && account.length>0) {
			view.getView('/widget/settings/api_settings?sub='+account, function(data) {
				$("#api_settings_container").html(data);
				Settings.addCustomValidation();
				Settings.initValidation();
			});
		}
	};


	Settings.initValidation = function () {
		$("#notifications_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				financeEmail: {maxlength:150},
				pricingEmail: {maxlength:150},
				opsEmail:     {maxlength:150},
				minBalance:   {number:true}
			}
		});

		$("#settings_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				encoding: {required: true},
				password: {required: true,
						   maxlength: 25,
						   minlength: 8,
						   minUpperCase: true,
						   minLowerCase: true,
						   minDigits: true,
						   alphanumeric: true,
						   api_secret: true
				},
				httpBaseUrlForMoPost: {url:true},
				httpBaseUrlForDnPost: {url:true}
			},
			messages: {
				password: {
					required: 'Secret must contain at least 8 characters, including at least one capital letter and at least one digit',
					maxlength: 'Secret must contain maximum 25 character',
					minLength: 'Secret must contain at least 8 characters',
					minUpperCase: 'Secret must contain at least one upper case character',
					minLowerCase: 'Secret must contain at least one lower case character',
					minDigits: 'Secret must contain at least one digit',
					alphanumeric: 'Secret must contain only alphanumeric characters',
					api_secret: 'Secret must contain only alphanumeric characters, at least 8 characters, including at least one capital letter and at least one digit'
				}
			}
		});
	};


	Settings.addCustomValidation = function () {
		$.validator.addMethod("api_secret", function(value, element, thresholds) {
			var allowdCharsRegex = /^[0-9a-zA-Z]+$/g,
			oldValue = $(element).attr('data-old'),
			Utils = Settings.utils;

			if(oldValue && oldValue === value){
				return true;
			}
			if(
					Utils.countUpperCase(value) < thresholds.minUpperCase ||
					Utils.countLowerCase(value) < thresholds.minLowerCase ||
					Utils.countDigits(value) < thresholds.minDigits ||
					$.trim(value).length < thresholds.minLength ||
					!allowdCharsRegex.test(value) 
			){
				return false;
			}
			return true;
		}, '');

		$.validator.addMethod("minUpperCase", function (value, element) {
			var oldValue = $(element).attr('data-old');
			if(oldValue && oldValue === value){
				return true;
			}
			return Settings.utils.countUpperCase(value) >= 1;
		}, '');

		$.validator.addMethod("minLowerCase", function (value, element) {
			var oldValue = $(element).attr('data-old');
			if(oldValue && oldValue === value){
				return true;
			}
			return Settings.utils.countLowerCase(value) >= 1;
		}, '');

		$.validator.addMethod("minDigits", function (value, element) {
			var oldValue = $(element).attr('data-old');
			if(oldValue && oldValue === value){
				return true;
			}
			return Settings.utils.countDigits(value) >= 1;
		}, '');

		$.validator.addMethod("alphanumeric", function (value, element) {
			var oldValue = $(element).attr('data-old');
			if(oldValue && oldValue === value){
				return true;
			}
			return /^[0-9a-zA-Z]+$/g.test(value);
		}, '');
		
	};

})(jQuery);


Settings.utils = (function(){

	var doCount = function(value, regex){
		var i,count=0,len=value.length;
		for(i=0;i<len;i++) {
			if(regex.test(value.charAt(i))) {
				count++;
			}
		}
		return count;
	},

	countUpperCase = function(value){
		return doCount(value, /[A-Z]/);
	},


	countLowerCase = function(value){
		return doCount(value, /[a-z]/);
	},

	countDigits = function(value){
		return doCount(value, /[0-9]/);
	},


	countSymbols = function(value){
		return doCount(value, /[`!"\$%\^&\*\(\)_\-\+={\[}\]:;@'~#\\\|<,>\.\?\/]/);
	};


	return {
		countUpperCase : countUpperCase,
		countLowerCase : countLowerCase,
		countSymbols : countSymbols,
		countDigits : countDigits
	};

}());
