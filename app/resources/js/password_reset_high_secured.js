
function PasswordExpiredReset() {
}

(function($) {
	
	$("body").ready(function() {
		$("#password_form").validate({
			errorElement: "p",
			wrapper: "span",	
			errorPlacement: function(error, element) {
				error.addClass("error");
				element.after(error);
			},
			rules: {
				password: {required:true,minlength:12, maxlength:40},				
				confirmPassword: {required:true,minlength:12, maxlength:40}	
			},
			messages: {
				password: "Provide a password of at least 12 characters, including at least one capital letter, one lowercase letter, one number and one symbol",
				confirmPassword : "Provide a password of at least 12 characters, including at least one capital letter, one lowercase letter, one number and one symbol"
			}
	      });
	});
	
})(jQuery);