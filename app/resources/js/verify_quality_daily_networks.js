
function VerifyQualityDailyNetworks() {
}

(function($) {
	$("body").ready(function() {

		$("#select_type").change(function(){
			if ($("#date_input").val()!='')
				VerifyQualityDailyNetworks.reloadView($("#select_type").val(),$("#date_input").val());
		});

		$("#date_input").change(function(){
			if ($("#date_input").val()!='')
				VerifyQualityDailyNetworks.reloadView($("#select_type").val(),$("#date_input").val());
		});

		//Cross-Browser solution Form Input Date Type
		function inputDateFix() {
			webshims.setOptions('waitReady', false);
			webshim.setOptions("forms-ext", {
				replaceUI: "auto",
				types: "date",
				date: {
					startView: 2,
					openOnMouseFocus: true,
					classes: "show-week"
				},
				number: {
					calculateWidth: false
				},
				range: {
					classes: "show-activevaluetooltip"
				}
			});
			webshims.polyfill('forms forms-ext');
			$.webshims.formcfg = {
					en: {
						dFormat: '-',
						dateSigns: '-',
						patterns: {
							d: "dd-mm-yy"
						}
					}
			};
		}
		inputDateFix();
	});

	VerifyQualityDailyNetworks.reloadView = function (type,date) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		view.getView('/widget/verify_quality/daily/networks/view/'+date+'?t='+type, function(data) {
			$("#reports_wait_container").hide();
			$("#reports_view_container").html(data);
		});
	};

})(jQuery);
