
function ConversionsNetworksDownload() {
}

(function($) {

	$("body").ready(function() {

		$("#start_input").change(function() {
			if ($("#start_input").val()!='' && $("#end_input").val()!='') {
				$('#networks_download_link').attr("href",$('#download_link_prefix').val()+'?a='+$("#select_account").val()+'&d1='+$("#start_input").val()+'&d2='+$("#end_input").val()+'&t=');
				$('#networks_download_link').show();
			}
		});
		$("#end_input").change(function() {
			if ($("#start_input").val()!='' && $("#end_input").val()!='') {
				$('#networks_download_link').attr("href",$('#download_link_prefix').val()+'?a='+$("#select_account").val()+'&d1='+$("#start_input").val()+'&d2='+$("#end_input").val()+'&t=');
				$('#networks_download_link').show();
			}
		});

		//Cross-Browser solution Form Input Date Type
		function inputDateFix() {
			webshims.setOptions('waitReady', false);
			webshim.setOptions("forms-ext", {
				replaceUI: "auto",
				types: "date",
				date: {
					startView: 2,
					openOnMouseFocus: true,
					classes: "show-week"
				},
				number: {
					calculateWidth: false
				},
				range: {
					classes: "show-activevaluetooltip"
				}
			});			
			webshims.polyfill('forms forms-ext');
			$.webshims.formcfg = {
					en: {
						dFormat: '-',
						dateSigns: '-',
						patterns: {
							d: "dd-mm-yy"
						}
					}
			};
		}
		inputDateFix();

	});

})(jQuery);

