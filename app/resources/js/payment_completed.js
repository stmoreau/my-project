function PaymentCompleted() {
}

(function($) {

    $("body").ready(function() {
        PaymentCompleted.switchInit();
        PaymentCompleted.switchEvents();
    });

    PaymentCompleted.switchInit = function(){
        $(".switch").bootstrapSwitch({size: 'small'});
    };

    PaymentCompleted.switchEvents = function(){
        $('[name="savePaymentMethod"]').on('switchChange.bootstrapSwitch', function(event, state) {
            if(state){
                admin.savePaymentInstrument($("#paymentInstrumentId").val(), 1);
            }else{
            	admin.savePaymentInstrument($("#paymentInstrumentId").val(), 0);
            }
        });
    };

})(jQuery);