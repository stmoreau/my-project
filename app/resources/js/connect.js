
function Connect() {
}

(function($) {

	$("body").ready(function() {

		$("#connect_form").validate({

			submitHandler: function(form) {
				admin.authorizeApp($("#oauth_token").val(),$("#oauth_callback").val(), function(ret) {
					if (ret!=null) {
						window.location.href=ret;
					}
				});
				return false;
			}
		});

	});

})(jQuery);

