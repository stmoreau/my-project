
function Dashboard() {
}

(function($) {
	
	$("body").ready(function() {

		$.jqplot.config.enablePlugins = true;
		var plot1;

		
		Menu.displayButtons("#menu_dashbaord");

		Dashboard.enableGettingStarted();
		if ( $('#_not_used').is('*') ) { 
			$('#getting_started_trigger').click();
		}
		if ( $('#_activation').is('*') ) { 
        	$('#welcome_message').slideDown('fast');
		}
		
		if ( $('#bulletin_close_link').is('*') ) { 
			$('#bulletin_close_link').click(function(){
				admin.closeBulletin(function() {
					$('#bulletin_message').slideUp('fast');
				});
			});
		}
		
		
		Menu.enableSettings($("#_uid").val());

		Dashboard.loadLastChart($("#_json_data").val());

		$("#select_type").change(function(){
			Dashboard.showSummary($("#select_type").val());
		});	
		
	});

	Dashboard.showSummary = function(type) {
		view.getView('/widget/get_last_activity_report?t='+type, function(data) {
			$("#last_activity_container").html(data);
			Dashboard.loadLastChart($("#_json_data").val());
		});
	};

	Dashboard.enableGettingStarted = function() {
          $('#getting_started_trigger').click(function(){
                 if ($('#welcome_message').is(":hidden")) {
                  	$('#welcome_message').slideDown('fast');
          		} else {
                  $('#welcome_message').slideUp('fast');
          		}
          });
          
          $('#get_started_close').click(function() {
                  $('#welcome_message').slideUp('fast');
          });
	  };	
	
	Dashboard.loadLastChart = function(data) {
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined')
		    	  plot1.replot( { resetAxes: true } );
		  });
		 	$('#chartdiv').html('');
		  
		 var chart=jQuery.parseJSON(data);

			if (chart!=null && chart!='') {
				plot1 = $.jqplot('chartdiv', [chart], {
					//Options
	    			axesDefaults: {
	    				tickOptions: {
	    					showMark: false
	    				}
	    			},
	    			axes: {
	    				xaxis: {
	    					renderer:$.jqplot.DateAxisRenderer,
	    					tickRenderer:$.jqplot.CanvasAxisTickRenderer,
	    					tickOptions: {
	    						angle: -90,
	    						fontFamily: "Arial",
	    						fontSize: "8pt",
	    						formatString: "%d %b %Y",
	    						tickInterval: "1 day"
	    					}
	    				},
						yaxis: {
							min: 0,
							tickOptions:{formatString:'%d'}
						}
	    			},
	    			seriesDefaults: {
	    				color: "#35b30e",
	                	shadow: false,
	                	markerOptions: {
	            			shadow: false
	            		}
	            	},
	            	grid: {
	            		gridLineColor: "#e8e8e8",
	            		background: "#ffffff",
	            		borderColour: "#e8e8e8",
	            		borderWidth: 0,
	            		shadow: false
	            	},
					highlighter: {
						show: true,
						sizeAdjust: 7,
						showTooltip: true,
						tooltipLocation: "n",
						fadeTooltip: true,
						tooltipFadeSpeed: "fast",
						tooltipOffset: 3,
						tooltipAxes: "y",
						useAxesFormatters: true
					}     	
	
				});
			}
		
	};	
})(jQuery);	
  