function Verify() {}

(function($) {

	$("body").ready(function() {
		Verify.customValidator();
		Verify.verifySubmitButton();
		Verify.inputSelect();
		Verify.autoTab();
		//Verify.autoTabInit();
	});


	Verify.autoTab = function() {
		$.autotab({ tabOnSelect: true });
		$('.number').autotab('filter', 'number');
		$('input[type="number"]')
		.autotab({
			maxlength: 1,
			'filter': 'number'
		}).on('click', function(event) {
			event.preventDefault();
			/* Act on the event */
			var $this = $(this);
			$this.autotab('restore');
			if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
				$this.autotab('disable');
			}
		});
		webshim.setOptions("forms-ext", false);
		webshims.polyfill('forms forms-ext', false);
	};
	/*
        Verify.autoTabInit = function() {
			$('input[type="number"], .input-number')
			.autotab({
				maxlength: 1}).on('click', function(event) {
					event.preventDefault();

					var $this = $(this);
					$this.autotab('restore');
					if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
						$this.autotab('disable');
					}
				});
			webshim.setOptions("forms-ext", false);
			webshims.polyfill('forms forms-ext', false);
		};
	 */
	Verify.customValidator = function() {
		$.formUtils.addValidator({
			name : 'verify',
			validatorFunction : function(value, $el, config, language, $form) {
				if (/^\d+$/.test(value) !== true){
					return false;
				}else{
					return true;
				}
			}
		});
	};    

	Verify.verifySubmitButton = function() {
		$("#verifySubmitButton").on("click", function () {
			Verify.validateInit();
		});
	}; 

	Verify.validateInit = function() {
		var msg = 'One or more of the fields are empty';
		var rules = {
				form: '#form-verify',
				errorMessagePosition : $('#error-message-wrapper'),
				validate: {
					'verify1': {
						validation: 'verify',
						'error-msg': msg
					}, 
					'verify2': {
						validation: 'verify',
						'error-msg': msg
					},
					'verify3': {
						validation: 'verify',
						'error-msg': msg
					},
					'verify4': {
						validation: 'verify',
						'error-msg': msg
					}    
				},
				onError : function() {
					$(this.errorMessagePosition).empty().append('<div class="form-error">'+msg+'</div>');
				},
				onSuccess : function() {
					Verify.concatCode();
				}
		};

		$.validate({
			modules : 'jsconf',
			onModulesLoaded : function() {
				$.setupValidation(rules);
			}
		});

		$(rules.form).bind('validation', function(evt, isValid) { 
			if(isValid){
				$(rules.errorMessagePosition).find('.form-error').remove();                    
				if($('.form-group-verify input').hasClass('error')){
					$(rules.errorMessagePosition).append('<div class="form-error">'+msg+'</div>')
				}
			}else{
				$(rules.errorMessagePosition).find('.form-error').last().remove();
			}  
		});

		$(rules.form).on('keyup', function(evt) { 
			var value = $(evt.target).val();    
			if (/^\d+$/.test(value) !== true && value !== $('[type="submit"]').val()){
				rules.onError();
				$(evt.target).closest('.form-group-verify').removeClass('has-success').addClass('has-error');
			}else{
				$(this.errorMessagePosition).find('.form-error').remove(); 
				$(evt.target).removeClass('error').addClass('valid').removeAttr('style').closest('.form-group-verify').removeClass('has-error').addClass('has-success');
				if($('.form-group-verify input').hasClass('error')){
					$(rules.errorMessagePosition).find('.form-error').remove();     
					$(rules.errorMessagePosition).append('<div class="form-error">'+msg+'</div>')
				}else{
					$(rules.errorMessagePosition).empty();
				}
				$(evt.target).closest('.input-verify').next().find('input').focus().select();
			}
		});
	};

	Verify.concatCode = function() {
		$('.input-verify input').attr('disabled', 'disabled');
		$('#code').val($('#verify1').val() + $('#verify2').val() + $('#verify3').val() + $('#verify4').val());
	}; 

	Verify.inputSelect = function() {
		$("input[type='text']").on("click", function () {
			$(this).select();
		});
	};

})(jQuery);
