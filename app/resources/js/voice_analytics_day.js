
function VoiceReportsNetwork() {
}

(function($) {
	
	$("body").ready(function() {

		var plot1;

		VoiceReportsNetwork.loadChart($("#select_type").val(),$("#select_date").val(),null);

		$("#select_type").change(function() {
			VoiceReportsNetwork.reloadDates($("#select_type").val(),$("#select_account").val());
			VoiceReportsNetwork.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		});
		
		$("#select_date").change(function(){
			VoiceReportsNetwork.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		});				
		
		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				VoiceReportsNetwork.reloadDates($("#select_type").val(),$("#select_account").val());
			});				
		}
		
		VoiceReportsNetwork.enableSorting();
	});
	
	VoiceReportsNetwork.reloadDates = function (type,account) {
		view.getView('/widget/voice/get_dates?t='+type+'&a='+account, function(data) {
			$("#reports_date_container").html(data);
			$("#reports_view_container").html('');
			$("#chartdiv").html('');
			$("#select_date").change(function(){
				VoiceReportsNetwork.reloadView($("#select_type").val(),$("#select_date").val(),account);
			});	
		});
	}

	VoiceReportsNetwork.reloadView = function (type,date,account) {
		$("#chart").hide();
		$("#chartdiv").html('');
		$('#network_download_link').hide();
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		$("#chartdiv").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/voice/networks/view/'+date+'?t='+type+'&a='+account, function(data) {
				$("#reports_view_container").html(data);
				VoiceReportsNetwork.enableSorting();
				VoiceReportsNetwork.loadChart(type,date,account);
				$("#reports_wait_container").hide();
			});
		} else {
			view.getView('/widget/voice/networks/view/'+date+'?t='+type, function(data) {
				$("#reports_view_container").html(data);
				VoiceReportsNetwork.enableSorting();
				VoiceReportsNetwork.loadChart(type,date,null);
				$("#reports_wait_container").hide();
			});
		}
	}

	VoiceReportsNetwork.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;
		
	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });
 
        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);	 
        });
	}	
	
	VoiceReportsNetwork.loadChart = function(type,date,account) {
		$('#chartdiv').html('');
		$("#chart").hide();
		$('#network_download_link').hide();
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined')
		    	  plot1.replot( { resetAxes: true } );
		  });
		admin.getAccountNetworkReport(type,date,account,function(chart) {
			if (chart!=null && chart!='') {
				$("#chart").show();
				$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
				$('#network_download_link').show();
				plot1 = $.jqplot('chartdiv', [chart], {
					  series:[{renderer:$.jqplot.BarRenderer,
						  		rendererOptions: {
						  				barWidth: 5,
						  				color: "#35b30e",
					                	shadow: false,
					                	markerOptions: {
					            			shadow: false
					            		}
						  		}}],
					  axesDefaults: {
					      tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
					      tickOptions: {
					        angle: -30
					      }
					  },
					  axes: {
					    xaxis: {
					      renderer: $.jqplot.CategoryAxisRenderer
					    },
					    yaxis: {
					     min: 0,
					     tickOptions:{formatString:'%d'},
					      autoscale:true
					    }
					  },
		            	grid: {
		            		gridLineColor: "#e8e8e8",
		            		background: "#ffffff",
		            		borderColour: "#e8e8e8",
		            		borderWidth: 0,
		            		shadow: false
		            	},
						highlighter: {
							show: true,
							sizeAdjust: 7,
							showTooltip: true,
							tooltipLocation: "n",
							fadeTooltip: true,
							tooltipFadeSpeed: "fast",
							tooltipOffset: 3,
							tooltipAxes: "y",
							useAxesFormatters: true
						}
				});
			}
		});
	}

})(jQuery);	
  