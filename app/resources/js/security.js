
(function($) {
		var token = $("meta[name='_csrf']").attr("content");
    	var headerName = $("meta[name='_csrf_header']").attr("content");
    	var csrfHeader = {}
    	csrfHeader[headerName]=token;
    	dwr.engine.setHeaders(csrfHeader);

    	$(document).ajaxSend(function(e, xhr, options) {
	        xhr.setRequestHeader(headerName, token);
	    });
})(jQuery);	


(function($) {
	function errh(msg, exc) {
		if(exc.javaClassName.indexOf("HackerDeniedException") != -1) {
			window.location.href="/sign-in"
		}
	}
	dwr.engine.setErrorHandler(errh);
})(jQuery);	

window.onerror = function(message, source, lineno, colno, error) { 
	var data = {
			UA : navigator.userAgent,
			MSG : message,
			FILE : toFilename( source ),
			LINE : lineno,
			COLUMN : colno
	};
	$.post((isLocalhost() ? '/customer' : '') + '/log/js/error', {error : JSON.stringify(data)});
	function isLocalhost(){
		return location.host === 'localhost:8080';
	}
	function toFilename(file){
		return file.substr(file.lastIndexOf('/') + 1, file.length);
	}
};