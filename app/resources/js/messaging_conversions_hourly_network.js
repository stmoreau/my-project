
function ConversionsHourlyNetwork() {
}

(function($) {
	
	$("body").ready(function() {

		$("#select_date").change(function(){
			ConversionsHourlyNetwork.reloadView($("#select_date").val(),$("#select_account").val());
		});		
				
		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				ConversionsHourlyNetwork.reloadView($("#select_date").val(),$("#select_account").val());
			});				
		}		
		
		ConversionsHourlyNetwork.enableSorting();
	});
	

	ConversionsHourlyNetwork.reloadView = function (date,account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/conversions/hourly/network/'+$("#_network_id").val()+'/view/'+date+'?a='+account, function(data) {
				$("#reports_view_container").html(data);
				ConversionsHourlyNetwork.enableSorting();
				$("#reports_wait_container").hide();
			});
		} else {
			view.getView('/widget/conversions/hourly/network/'+$("#_network_id").val()+'/view/'+date, function(data) {
				$("#reports_view_container").html(data);
				ConversionsHourlyNetwork.enableSorting();
				$("#reports_wait_container").hide();
			});
		}
	}

	ConversionsHourlyNetwork.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;
		
	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });
 
        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);	 
        });
	}	
	

	
})(jQuery);	
  