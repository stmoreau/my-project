
function NIReportsMonthly() {
}

(function($) {

	$("body").ready(function() {

		$("#select_type").change(function(){
			NIReportsMonthly.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		});
		$("#select_date").change(function(){
			NIReportsMonthly.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
		})

		if ( $('#select_account').is('*') ) {
			$("#select_account").change(function(){
				NIReportsMonthly.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val());
			});
		}
		if ( $('#report_table').is('*') ) {
			$('#network_download_link').attr("href",$('#download_link_prefix').val()+$("#select_date").val());
			$('#network_download_link').show();
		} else $('#network_download_link').hide();

		NIReportsMonthly.enableSorting();

	});

	NIReportsMonthly.reloadView = function (type,date,account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		view.getView('/widget/ni/month/view/'+date+'?t='+type+'&a='+account, function(data) {
			$("#reports_view_container").html(data);
			$("#reports_wait_container").hide();
			NIReportsMonthly.enableSorting();
			if ( $('#report_table').is('*') ) {
				$('#network_download_link').attr("href",$('#download_link_prefix').val()+date+'?t='+type);
				$('#network_download_link').show();
			} else $('#network_download_link').hide();
		});
	}

	NIReportsMonthly.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;

	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });

        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);
        });
	}

})(jQuery);
