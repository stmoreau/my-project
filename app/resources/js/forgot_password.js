
function ForgotPassword() {
}

(function($) {

	$("body").ready(function() {

        var windowWidth = $(window).outerWidth();

		$("#forgotForm").validate({
				errorElement: "span",
				errorPlacement: function(error, element) {
					error.addClass("inline-error");
                    error.insertAfter(element);
				},
				rules: {
					forgot_input: {required:true,email:true,maxlength:50}
				}
	    });

	});

})(jQuery);

