function ProfilePassword() {
}

(function($) {
	
	$("body").ready(function() {
		
		Menu.displayButtons(null);
		
		Menu.enableSettings($("#_uid").val());

		$("#profile_password_form").validate({
			errorElement: "p",
			wrapper: "span",	
			errorPlacement: function(error, element) {
				error.addClass("error");
				element.after(error);
			},
			rules: {
				existingPassword: {required:true,minlength:6}, 
				password: {required:true,minlength:12, maxlength:40},				
				confirmPassword: {required:true,minlength:12, maxlength:40}			
			},
			messages: {
				existingPassword: "Specify your existing password",
				password: "Provide a password of at least 12 characters, including at least one capital letter, one lowercase letter, one number and one symbol",
				confirmPassword : "Provide a password of at least 12 characters, including at least one capital letter, one lowercase letter, one number and one symbol"
			}
	      });

	});
})(jQuery);	