function NILookup() {
}

(function($) {

	$("body").ready(function() {

		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

		NILookup.enableHlrSearch();
	});

	NILookup.enableHlrSearch = function(){
		$("#hlr_form").validate({
			rules: {
				hlr_msisdn_input: {number:true, minlength:6, maxlength:20, required: true, regex: /^(?:[0-9] ?){6,15}[0-9]$/}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error products-search-pages-error");
				error.appendTo(element.parent("div"));
			},
			messages: {
				hlr_msisdn_input: 'Please enter a valid phone number.'
			},
			submitHandler: function(form) {
				$("#hlr_submit_container").hide();
				$("#hlr_wait_container").show();
				$("#hlr_result_container").empty();
				view.getView('/widget/search/hlr-lookup/'+$("#hlr_msisdn_input").val(), function(data) {
					if (data!=null && data!='void') {
						$("#hlr_result_container").html(data);
					} else {
						$("#hlr_result_container").html('');
					}
					$("#hlr_submit_container").show();
					$("#hlr_wait_container").hide();
				});
				return false;
			}
		});
	};

})(jQuery);