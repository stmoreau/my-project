
function PhoneBook() {
}

(function($) {

	$("body").ready(function() {
		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

		PhoneBook.enableRequestUpload();
		PhoneBook.enableRequestActions();
		PhoneBook.enablePagination();
		PhoneBook.enablePhoneBookNumerSearch();

	});

	PhoneBook.enableRequestActions = function() {
		$('.phonebook_request_cancel_link').each(function(i,o) {
			$(this).click(function() {
				admin.cancelPhoneBookRequest($(this).attr('rel'), function(ret) {
					if (ret==true) {
						PhoneBook.reloadList(1);
					} else alert('Internal processing error!');
				});
			});
		});
	};

	PhoneBook.enablePagination = function() {
		$(".phonebook_request_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				PhoneBook.reloadList(index);
			});
		});
	}

	PhoneBook.reloadList = function(index) {
		view.getView('/widget/search/phonebook_request?p='+index, function(data) {
			$("#phonebook_list_container").html(data);
			PhoneBook.enablePagination();
			PhoneBook.enableRequestActions();
		});
	}

	PhoneBook.enableRequestUpload = function() {

		$("#file_help_link").click(function() {
			$("#file_help_container").toggle();
		});

		$("#phonebook_upload_form").validate({
			rules: {
				phonebook_upload_country: {required:true},
				phonebook_upload_file: {required:true}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				if (element.attr("name") == "phonebook_upload_country") {
					error.addClass("help-block form-error signup-nojs-error phonebook-country-error");

				} else if (element.attr("name") == "phonebook_upload_file") {
					error.addClass("help-block form-error signup-nojs-error phonebook-file-upload-error");
				}
				error.appendTo(element.parent("div"));
			},
			messages: {
				phonebook_upload_country: 'Please enter choose a country.',
				phonebook_upload_file:    'Please choose a file to upload.'
			},
			submitHandler: function(form) {
				$("#phonebook_upload_submit_container").hide();
				$("#phonebook_upload_wait_container").show();
				var file = dwr.util.getValue('phonebook_upload_file');
				admin.uploadUserPhoneBookRequest($("#phonebook_upload_country").val(),file, function(data) {
					if (data>0) {
						$("#phonebook_upload_result_container").html('<p>Success: '+ data + '</p>');
						PhoneBook.reloadList(1);
					} else {
						$("#phonebook_upload_result_container").html('<p class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error</p>');
					}
					$("#phonebook_upload_submit_container").show();
					$("#phonebook_upload_wait_container").hide();
				});
				return false;
			}
		});
	};

	PhoneBook.enablePhoneBookNumerSearch = function() {

		$("#phonebook_number_search").validate({
			rules: {
				phonebook_number_country: {required:true},
				phonebook_number_msisdn: {number:true, maxlength:20, regex: /^(?:[0-9] ?){6,15}[0-9]$/}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				if (element.attr("name") == "phonebook_number_country") {
					error.addClass("help-block form-error signup-nojs-error phonebook-search-country-error");

				} else if (element.attr("name") == "phonebook_number_msisdn") {
					error.addClass("help-block form-error signup-nojs-error phonebook-search-msisdn-error");
				}
				error.appendTo(element.parent("div"));
			},
			messages: {
				phonebook_number_country: 'Please enter choose a country.',
				phonebook_number_msisdn: {
					required: 'Please provide a phone number.',
					regex: 'Please provide a valid phone number.',
					maxlength: 'Please provide a phone number with maximum 20 digits.'
				}
			},
			submitHandler: function(form) {
				$("#phonebook_number_submit_container").hide();
				$("#phonebook_number_wait_container").show();
				$("#phonebook_number_result_container").html('');
				view.getView('/widget/search/phonebook_number?co='+$("#phonebook_number_country").val()+'&prefix='+$("#phonebook_number_msisdn").val(), function(data) {
					if (data!=null && data!='void') {
						$("#phonebook_number_result_container").html(data);
						$('#collapseDetails').removeClass('collapse');
						PhoneBook.enablePhoneBookNumberSearchPagination($("#phonebook_number_country").val(), $("#phonebook_number_msisdn").val());
					} else {
						$("#phonebook_number_result_container").html('');
					}
				});
				$("#phonebook_number_submit_container").show();
				$("#phonebook_number_wait_container").hide();
				return false;
			}
		});
	};

	PhoneBook.enablePhoneBookNumberSearchPagination = function(country, account, msisdn) {
		$(".phonebook_number_search_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				view.getView('/widget/search/phonebook_number?co='+country+'&i='+index+'&prefix='+msisdn, function(data) {
					if (data!=null && data!='void') {
						$("#phonebook_number_result_container").html(data);
					} else {
						$("#phonebook_number_result_container").html('');
					}
				});
				return false; 
			});
		});
	};

})(jQuery);
