
function Profile() {
}

(function($) {

	$("body").ready(function() {

		$("#profile_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				email:     {required:true, email:true, maxlength:50},
				firstName: {required:true, maxlength:50, regex: /^[a-zA-Z]+[\_\.\-\ \'a-zA-Z]*$/},
				lastName:  {required:true, maxlength:50, regex: /^[a-zA-Z]+[\_\.\-\ \'a-zA-Z]*$/},
				im:        {maxlength:50}
			},
			messages: {
				email:     'Please provide a valid email address.',
				firstName: 'Please provide your first name.',
				lastName:  'Please provide your last name'
			}
		});

		$('.popover-hint').popover({
			html: true,
			template: '<div class="popover popover-normal"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div></div>'
		});

		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

	});

})(jQuery);

