(function () {
    'use strict';

    var utms = 'utm_source',
        utmm = 'utm_medium',
        utmt = 'utm_term',
        utmcn = 'utm_content',
        utmcm = 'utm_campaign',
        days = 365 * 24 * 60 * 60 * 1000;

    if (!getCookie(utms)) {
        var query = window.location.search.substring(1),
            vars = query.split('&'),
            urlHasUtmSource = false,
            isUtmContentInURL = false;
        for (var outer = 0; outer < vars.length; outer++) {
            var pair = vars[outer].split('='),
                key = decodeURIComponent(pair[0]);
            urlHasUtmSource = (key == utms);
            if (urlHasUtmSource) {
                setCookie(utms, decodeURIComponent(pair[1]));

                for (var inner = 0; inner < vars.length; inner++) {
                    pair = vars[inner].split('=');
                    key = decodeURIComponent(pair[0]);

                    switch (key) {
                        case utmm:
                        case utmt:
                        case utmcm:
                            setCookie(key, decodeURIComponent(pair[1]));
                            break;
                        case utmcn:
                            isUtmContentInURL = true;
                            setCookie(key, decodeURIComponent(pair[1]));
                            break;
                    }
                }

                if ( ! isUtmContentInURL && document.referrer) {
                    setCookie(utmcn, document.referrer);
                }
                break;
            }
        }

        if (! urlHasUtmSource && document.referrer) {
            useReferralUrlToAddCookies(document.referrer);
        }
    }

    function useReferralUrlToAddCookies(refURL) {
        var refDomain = getDomain(refURL);
        if (refDomain && refDomain !== 'nexmo.com') {
            setCookie(utms, refDomain);

            if (starts_with(refDomain, 'google') ||
                starts_with(refDomain, 'bing') ||
                ends_with(refDomain, 'yahoo.com')
            ) {
                setCookie(utmm, 'organic');
            } else {
                setCookie(utmcn, refURL);
            }
        }
    }

    function getDomain(str) {
        var regexp = /(([a-z0-9\-]+)\.([a-z0-9]+([.a-z0-9])([.a-z0-9])+))/g;
        return regexp.exec(str)[1].replace('www.','');
    }

    function starts_with(haystack, needle) {
        return haystack.indexOf(needle) === 0;
    }

    function ends_with(haystack, needle) {
        return haystack.indexOf(needle) == haystack.length - (needle.length);
    }

    function setCookie(name, value) {
        var expires, date;
        if (days) {
            date = new Date();
            date.setTime(date.getTime() + (days));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = name + "=" + value + expires + ";path=/;domain=."+getDomain(window.location.href);
    }

    function getCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
        }
        return false;
    }

})();