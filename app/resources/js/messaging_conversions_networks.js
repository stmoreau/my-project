
function ConversionsNetworks() {
}

(function($) {
	
	$("body").ready(function() {

		$("#select_date").change(function(){
			ConversionsNetworks.reloadView($("#select_date").val(),$("#select_account").val());
		});				
		
		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				ConversionsNetworks.reloadView($("#select_date").val(),$("#select_account").val());
			});				
		}
		
		ConversionsNetworks.enableSorting();
	});
	

	ConversionsNetworks.reloadView = function (date, account) {
		$("#reports_wait_container").show();
		$("#reports_view_container").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/conversions/networks/view/'+date+'?a='+account, function(data) {
				$("#reports_view_container").html(data);
				ConversionsNetworks.enableSorting();
				$("#reports_wait_container").hide();
			});
		} else {
			view.getView('/widget/conversions/networks/view/'+date, function(data) {
				$("#reports_view_container").html(data);
				ConversionsNetworks.enableSorting();
				$("#reports_wait_container").hide();
			});
		}
	}

	ConversionsNetworks.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;
		
	    $("#report_table").bind("sortStart", function () {
            // remove the row with the class nosort
            pageRow = $(this).children("tbody").children("tr.nosort");
            $(this).children("tbody").children("tr.nosort").remove();
        });
 
        $("#report_table").bind("sortEnd", function () {
            // add the removed row
            $(this).children("tbody").append(pageRow);	 
        });
	}	
	

	
})(jQuery);	
  