
function Numbers() {
}

(function($) {

	$("body").ready(function() {

		Numbers.enableNumberCancel();
		Numbers.enableNumberUpdate();
		Numbers.enableNumberAccountPagination();
		Numbers.enableEditNumber();
		Numbers.showDeleteConfirmationPopup();

		Numbers.showCancelShortcodesCampaignPopup();
		Numbers.enableCancelCampaign();
		Numbers.enableCancelKeyword();
		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				Numbers.loadShortcodes();
			});
		}

	});

	Numbers.enableNumberFiltering = function() {
		$("#filter_number_form").validate({
			rules: {
				filter_country: {required:true},
				filter_input: {number:true,minlength:2,maxlength:4}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			messages: {
				filter_country: "Please choose a country.",
				filter_input: {
					number: 'Please enter a valid number.',
					maxlength: 'Please enter maximum 4 digits.'
				}
			},
			submitHandler: function(form) {
				view.getView('/widget/account/numbers?co='+$('#filter_country').val()+'&n='+$('#filter_input').val()+'&features='+$('#filter_features').val(), function(data) {
					if (data!=null && data!='void') {
						$("#numbers_filtering_results").html(data);
						Numbers.enableNumberAccountPagination();
					} else {
						$("#numbers_filtering_results").html('');
						alert("Your session has expired, please sign-in again!");
					}
				});
			}
        });
	};

	Numbers.loadNumbers = function() {
		view.getView('/widget/account/numbers', function(data) {
			$("#numbers_filtering_results").html(data);
			Numbers.enableNumberAccountPagination();
		});
		
	};

	Numbers.enableNumberCancel = function() {
		$(document).on("click", ".number_cancel_link", function(event){	
			var splitRes = $(this).attr('rel').split(":");
			admin.cancelNumber(splitRes[0], splitRes[1], function(ret) {
				Numbers.loadNumbers();
				$('#confirmationModal').modal('hide');
				$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
			});
		});
	};

	Numbers.showDeleteConfirmationPopup = function() {
		$(document).on("click", ".cancelNumberModal", function(event){

			var splitRes = $(this).attr('id').split(":");
			var co = splitRes[0];
			var msisdn = splitRes[1];

			view.getView('/widget/show_delete_popup?co='+co+'&msisdn='+msisdn, function(data) {
				if (data) {
					$('#confirmationModal').html(data);
					$('#confirmationModal').modal('show');
				}
			});
			return false;
		});
	};

	Numbers.enableNumberUpdate = function() {
		Numbers.enableNumberFiltering();
		$(document).on("click", ".number_update_link", function(event){
			var splitRes = $(this).attr('rel').split(":");
			var co = splitRes[0];
			var msisdn = splitRes[1];
			var http = "";
			if ($('#http_'+co+'_'+msisdn).is('*')) http = $('#http_'+co+'_'+msisdn).val();
			var smpp = "";
			if ($('#smpp_'+co+'_'+msisdn).is('*')) smpp = $('#smpp_'+co+'_'+msisdn).val();
			var type = $('#meta_type_'+co+'_'+msisdn).val();
			var meta = $('#meta_'+co+'_'+msisdn).val();
			var status = $('#status_'+co+'_'+msisdn).val();
			$("#edit_number_update_btn").prop('disabled', true);
			$("#updating_settings_message").show();
			admin.updateNumber(co, msisdn ,http,smpp,'',type,meta,status, function(ret) {
				if (ret==0) {
					Numbers.loadNumbers();
					$('#myModal').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
				}
				else if (ret==2) alert("The configured URL is not responding, we are testing with GET and POST and expect a 200 status");
				else if (ret==3) alert("Make sure to enter a valid phone number including international dialing code");
				else if (ret==4) alert("Make sure to enter a valid SIP address");
				else if (ret==5) alert("You cannot use a Nexmo long virtual number");
				else if (ret==6) alert("You cannot use this domain.");
				else if (ret==7) alert("During the trial period you can only forward to the number you have used for registration.");
				$("#edit_number_update_btn").prop('disabled', false);
				$("#updating_settings_message").hide();
			});
		});
	};

	Numbers.enableNumberAccountPagination = function() {
		$(".number_account_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				view.getView('/widget/account/numbers?p='+index+'&co='+$('#filter_country').val()+'&n='+$('#filter_input').val(), function(data) {
					if (data!=null && data!='void') {
						$("#numbers_filtering_results").html(data);
						Numbers.enableNumberAccountPagination();
					} else {
						$("#numbers_filtering_results").html('');
						alert("Your session has expired, please sign-in again!");
					}
				});
				return false;
		    });
		});
	};

	Numbers.enableEditNumber = function () {
		$(document).on("click", ".editSettingsModal", function(event){

			var splitRes = $(this).attr('id').split(":");
			var co = splitRes[0];
			var msisdn = splitRes[1];

			view.getView('/widget/get_number?co='+co+'&msisdn='+msisdn, function(data) {
				if (data) {
					$('#myModal').html(data);
					$('#myModal').modal('show');
				}
			});
			return false;
		});
	};

	//Shortcodes
	Numbers.showCancelShortcodesCampaignPopup = function() {
		$(document).on("click", ".cancelCampaignModal, .cancelKeywordCampaignModal", function(event){
			view.getView('/widget/show_campaign_cancel_popup?id='+$(this).attr('id'), function(data) {
				if (data) {
					$('#confirmationModal').html(data);
					$('#confirmationModal').modal('show');
				}
			});
			return false;
		});
	};

	Numbers.enableCancelCampaign = function() {
		$(document).on("click", ".campaign_cancel_link", function(event){
			admin.removeUserShortcode($(this).attr('id'), function(ret) {
				window.location.href="your-numbers";
			});
			return false;
		});
	};

	Numbers.enableCancelKeyword = function() {
		$(document).on("click", ".campaign_cancel_keyword_link", function(event){
    		var splitRes = $(this).attr('id').split(":");
			admin.removeUserShortcodeKeyword(splitRes[0], splitRes[1], splitRes[2], function(ret) {
				window.location.href="your-numbers";
			});
			return false;
		});
	};

	Numbers.loadShortcodes = function() {
		view.getView('/widget/usshortcode/account/list?a='+$("#select_account").val(), function(data) {
			$('#shortcodes_list_container').html(data);
		});
	};

})(jQuery);

