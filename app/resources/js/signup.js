function Signup() {
}

(function($) {

	$("body").ready(function() {
        Signup.customValidator();
        Signup.validateInit();
        Signup.bindEvents();
        Signup.removeCountry();
        Signup.passwordInfoShow();
        Signup.websiteInfoShow();
        Signup.phoneInfoShowHide();
        Signup.setFlags();
        Signup.submitSupport();
        Signup.submitSupportRegisteredPhone();
        Signup.submitSupportRegisteredEmail();
   });

    Signup.customValidator = function(){
        $.formUtils.addValidator(
        {
              name : 'password_custom',
              validatorFunction : function(value, $el, config, language, $form) 
              {
	                if (value === '') {
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-empty']);
	                    
	                }else if (value.length < 8 || value.length > 23 ){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-length']);
	                    
	                }else if (/[A-Z]/.test(value) !== true){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-capital']);
	                    
	                }else if (/\d/.test(value) !== true){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-digit']);
	                    
	                }else{                    
	                    return true;
	                }
              }
        });
        
        $.formUtils.addValidator(
        {
            name : 'phone_custom',
            validatorFunction : function(value, $el, config, language, $form) 
            {                          
            	  var phoneNumberWithOptionalSpaces = /^(?:[0-9] ?){6,15}[0-9]$/;
	              if (value === ''  ||  phoneNumberWithOptionalSpaces.test(value) !== true){
	                  $el.attr('data-validation-error-msg', config.validate.phone['error-msg']);
	                  $('.phone-info').addClass('hide');
	                  
	              }else{
	                  return true;
	              }
            }
        });
        
        $.formUtils.addValidator(
        {
            name : 'firstname_custom',
            validatorFunction : function(value, $el, config, language, $form) 
            {   
            	var firstNameValidator = /^[a-zA-Z0-9.'\-_\s]+$/; 
            	if (value === ''  ||  firstNameValidator.test(value) !== true){
            		$el.attr('data-validation-error-msg', config.validate.firstName['error-msg']);
	            }else{
	                return true;
	            }
            }
       });
        
       $.formUtils.addValidator(
       {
            name : 'lastname_custom',
            validatorFunction : function(value, $el, config, language, $form) 
            {   
            	var lastNameValidator = /^[a-zA-Z0-9.\-\s]+$/; 
            	if (value === ''  ||  lastNameValidator.test(value) !== true){
            		$el.attr('data-validation-error-msg', config.validate.lastName['error-msg']);
	            }else{
	                return true;
	            }
            }
       });
    };

    Signup.validateInit = function(){
        var rules = {
            form: '.form-validate',
            validate: {
                'firstName': {
                    validation: 'firstname_custom',
                    'error-msg': 'Provide your first name'
                },
                'lastName': {
                    validation: 'lastname_custom',
                    'error-msg': 'Provide your last name'
                },
                'email': {
                    validation: 'email',
                    'error-msg': 'Provide a valid email address'
                },
                'phoneCountry': {
                    validation: 'required',
                    'error-msg': 'Select a country'
                },
                'phone': {
                    validation: 'phone_custom',              
                    'error-msg': 'Provide a valid phone number'
                },
                'password': {
                    validation: 'password_custom',
                    'error-msg': 'default',
                    'error-msg-empty': 'Provide a password of at least 8 characters, including at least one capital letter and at least one number.',
                    'error-msg-length': 'Provide a password of between 8 and 23 characters',
                    'error-msg-capital': 'Include at least one capital letter in your password',
                    'error-msg-digit': 'Include at least one number in your password'
                },
                'website': {
                    validation: 'required',
                   'error-msg': 'Provide a valid website address'
                },
                'services':{
                   validation: 'validate_checkbox_group',
                   qty: '1-4',
                   'error-msg': 'Select which service or services you intend to use'
                },
                'reason': {
                    validation: 'required',
                   'error-msg': 'Provide a use case or explain your reason for wanting to use our services'
                }          
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });
        
        $('[name="services"]').bind('validation', function(evt, isValid) {
            if(!isValid){
                $('.verify-support-services-error').empty().append('<div class="form-error">'+rules.validate['services']['error-msg']+'</div>');
            }else{
                 $('.verify-support-services-error').empty();
            }
        });

    };

    Signup.bindEvents = function(){
    	$('form input').bind('validation', function(evt, isValid) { 
        	if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });
    };

    Signup.removeCountry = function(){
        $('[name="country"]').change(function(){
            if($(this).val() !== ''){
                $(this).closest('.form-group').find('.help-block').remove();
                $(this).closest('.form-group').removeClass('has-error');
                $(this).closest('.form-group').removeClass('has-error').addClass('has-success');
            } 
        });
    };

    Signup.passwordInfoShow = function(){ 
    	$('[name="password"]').on('focus', function(){
    		if(isHighSecuredEmailDomainValidationError){
        		$('.password-info').addClass('hide');
        	}else{
        		$('.password-info').removeClass('hide');
        	}
        });

        $('[name="password"]').on('keyup', function(){
            var pass = $('[name="password"]').val();
            if((pass.length >= 8) && (pass.length < 23) && (/[A-Z]/.test(pass) == true) && (/\d/.test(pass) == true)){
        		$('.password-info').addClass('hide');
        	}
        });
    };

    Signup.websiteInfoShow = function(){
        $('[name="website"]').on('focus', function(){
            $('.website-info').removeClass('hide');
        });
    };

    Signup.phoneInfoShowHide = function(){
    	$('[name="phone"]').on('focus', function(){
    		var phoneErrorFromClientValidation = $('#phone').next('.form-error').length;
    		
    		$('.phone-info').removeClass('hide');   		
    		$('#phoneError').addClass('hide'); 
    		
    		if(phoneErrorFromClientValidation){
    			$('.phone-info').addClass('hide');
    		}
    		
        });
    	
    	$('#phone').on('blur', function(){
    		$('.phone-info').addClass('hide');
        });
    	
    	$('#phone').on('keyup', function(){
    		if($('#phone').next('.form-error').length>0){
    			$('.phone-info').removeClass('hide');
    		}
        });

    };

    /** phone extension / country selection
     * 
     *  DEPENDENCY: <script>var nexmoContextPath = "${pageContext.request.contextPath}"</script>
     */
    Signup.setFlags = function(){
        function formatFlags (state) {
            if (!state.id) { return state.text; }
            var $ext = $(state.element).attr('data-ext');
            var $state = $('<span><img src="'+ nexmoContextPath +'resources/img/blank.gif" class="flag flag-' + state.element.value.toLowerCase() + '" /> ' + $ext + '</span>');
            return $state;
        }

        $(".countryCode").select2({
            templateSelection: formatFlags
        });
    };

    Signup.submitSupport = function(){
		$('#invalid_phone').click(function(event){
			event.preventDefault();
			$('#password').val("Password1");
			$('#registration-form').append($('<input type="hidden" name="_invalidPhone" value="" />'));
			$('#registration-form').submit();		
		});		
	};

	Signup.submitSupportRegisteredPhone = function(){
		$('#already_registered_phone').click(function(event){
			event.preventDefault();
			$('#password').val("Password1");
			$('#registration-form').append($('<input type="hidden" name="_alreadyRegisteredPhone" value="" />'));
			$('#registration-form').submit();		
		});		
	};
	
	Signup.submitSupportRegisteredEmail = function(){
		$('#already_registered_email').click(function(event){
			event.preventDefault();
			$('#password').val("Password1");
			$('#registration-form').append($('<input type="hidden" name="_alreadyRegisteredEmail" value="" />'));
			$('#registration-form').submit();		
		});		
	};
	
	Signup.submitSignOutForm = function() {
		$('#logoutForm').submit();
	}
	
})(jQuery);