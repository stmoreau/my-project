
function ShortcodeMarketing2() {
}

(function($) {

	$("body").ready(function() {

		jQuery.validator.addMethod("email_or_phone", function(value, element) {
			return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value) || /[0-9 -()+]+$/i.test(value);
		},"Please enter an email or a phone number");

		jQuery.validator.addMethod("phone", function(value, element) {
			return this.optional(element) || /[0-9 -()+]+$/i.test(value);
		},"Please enter a phone number");

		$("#marketing_form_2").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				confirmationMessage: {required:true,minlength:10,maxlength:160},
				programName	: {required:true,minlength:3,maxlength:45},			
				paramHelp: {email_or_phone: true,required:true,minlength:10,maxlength:45},
				paramStop: {email_or_phone: true,required:true,minlength:10,maxlength:100},
				messageFrequency: {required:true,minlength:1, maxlength:20},
				contactTandC:{required:true},
				contactEmail: {email: true, required:false},
				contactPhone: {required:false, phone: true}	
			},
			messages: {
				programName: {required:"Program Name is required",
					minlength:"Please enter at least 3 characters for Program Name",
					maxlength:"Please enter no more than 20 characters for Program Name"},
				paramHelp: {email_or_phone: "Please enter an email or a phone number for Support Info",
					required:"Support Info is required",
					minlength:"Please enter at least 10 characters for Program Name",
					maxlength:"Please enter no more than 100 characters for Support Info"},
					messageFrequency: {number: "Please enter a valid number for Msg Frequency",
						required:"Msg Frequency is required",
						minlength:"Please enter at least 1 character for Msg Frequency"}
			}
		});

	});

})(jQuery);

