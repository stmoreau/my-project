function TestPhoneNumbers() {
}

(function($) {

	$("body").ready(function() {

		TestPhoneNumbers.addCustomValidation();
		TestPhoneNumbers.enableTestForm();
	});

	TestPhoneNumbers.addCustomValidation = function () {
		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

	};

	TestPhoneNumbers.enableTestForm = function() {
		$("#test_form").validate({
			rules: {
				phoneNumberCountry: {required:true},
				phoneNumber: {required:true, minlength:8, maxlength:20, regex: /^(?:[0-9] ?){6,15}[0-9]$/}
			},
			messages: {
				phoneNumberCountry: 'Please choose the country of the phone number you want to add.',
				phoneNumber: 'Please provide a valid phone number.'
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			}
		});

	};

})(jQuery);

