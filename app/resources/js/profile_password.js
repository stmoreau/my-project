
function ProfilePassword() {
}

(function($) {

	$("body").ready(function() {

		$("#profile_password_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				existingPassword: {required:true,minlength:6},
				password: {required:true,minlength:8, maxlength:40, passwordCustom_capitalDigit: true, passwordCustom_digit: true},
				confirmPassword: {required:true,minlength:8, maxlength:40, passwordCustom_capitalDigit: true, passwordCustom_digit: true, confirmPasswordCustom: true}
			},
			messages: {
				existingPassword: 'Please enter your existing password',
				password: {
					required: 'Please provide a password of at least 8 characters, including at least one capital letter and at least one digit',
					minlength: 'Please provide a password of between 8 and 40 characters',
					maxlength: 'Please provide a password of between 8 and 40 characters',
					passwordCustom_capitalDigit: 'Please include at least one capital letter in your password',
					passwordCustom_digit: 'Please include at least one digit in your password'
				},
				confirmPassword: {
					required: 'Please provide a password of at least 8 characters, including at least one capital letter and at least one digit',
					minlength: 'Please provide a password of between 8 and 40 characters',
					maxlength: 'Please provide a password of between 8 and 40 characters',
					passwordCustom_capitalDigit: 'Please include at least one capital letter in your password',
					passwordCustom_digit: 'Please include at least one digit in your password',
					confirmPasswordCustom: 'Your new password does not match'
				}
			}
		});

		$.validator.addMethod("passwordCustom_capitalDigit", function (value, element) {
			return /[A-Z]/.test(value);
		}, '');

		$.validator.addMethod("passwordCustom_digit", function (value, element) {
			return /\d/.test(value);
		}, '');

		$.validator.addMethod("confirmPasswordCustom", function (value, element) {
			return value == $('#profile_newPassword').val();
		}, '');

	});

})(jQuery);

