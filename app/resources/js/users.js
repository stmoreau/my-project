
function Users() {
}

(function($) {

	$("body").ready(function() {

		Users.showUserForm();
		Users.enableCancelUser();
		Users.enablePagination();
	});

	Users.showUserForm = function() {
		$("#add_user_link").click(function() {
			view.getView('/widget/add_secondary_user', function(data) {
				$('#addUserModal').html(data);
				$('#addUserModal').modal('show');
				$("#restriction_user_add").asmSelect({addItemTarget: 'bottom',animate: true,highlight: true});
				Users.enableAddUser();
				Users.enableCancelUser();
			});
		});
	};

	Users.loadUsers = function() {
		view.getView('/widget/secondary_users', function(data) {
			$("#users_container").html(data);
			Users.showUserForm();
			Users.enableCancelUser();
			Users.enablePagination();
		});
	};

	Users.enableCancelUser =  function() {
		$('.cancel_user_link').each(function(i,o) {
			$(this).click(function() {
				admin.cancelSecondaryUser($(this).attr('rel'), function(ret) {
					if (ret==true) {
						Users.loadUsers();
					}
				});
			});
		});
	};

	Users.enableAddUser =  function() {
		$("#add_user_form").validate({
			rules: {
				first_user_add: {required:true,maxlength:50, regex: /^[a-zA-Z]+[\_\.\-\ \'a-zA-Z]*$/},
				last_user_add: {required:true,maxlength:50, regex: /^[a-zA-Z]+[\_\.\-\ \'a-zA-Z]*$/},
				email_user_add: {required:true,email:true,maxlength:50}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			/*success: function(error, element) {
				$(element).next('.icon-display').removeClass('fa-exclamation-triangle error-color').addClass('hide');
				error.removeClass('help-block form-error signup-nojs-error').addClass('hide');
			},*/
			submitHandler: function(form) {
				admin.addSecondaryUser($("#email_user_add").val(),$("#first_user_add").val(),$("#last_user_add").val(),$("#restriction_user_add").val(), function(ret) {
					if (ret==true) {
						$('#addUserModal').modal('hide');
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
						$('#user_add_result_container').html('<div style="padding: 10px 0; color:green; display: -webkit-inline-box"><img src="'+nexmoContextPath+'resources/img/green_check.png" alt="" style="margin-right:20px"> <p style="color: green">A temporary password has been sent to '+$("#email_user_add").val() + '</p></div>');
						Users.loadUsers();
					} else {
						$('#user_add_form_error_container').html('<p class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error: The email is already registered or the fields submitted are incorrect.</p>');
					}
				});
				return false;
			}
		});
	};

	$.validator.addMethod("regex", function (value, element, regexp) {
		var re = new RegExp(regexp);
		return re.test(value);
	}, '');


	Users.enablePagination = function() {
		$(".users_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				view.getView('/widget/secondary_users?p='+index, function(data) {
					$("#users_container").html(data);
					Users.showUserForm();
					Users.enableCancelUser();
					Users.enablePagination();
				});
				return false;
			});
		});
	};

})(jQuery);
