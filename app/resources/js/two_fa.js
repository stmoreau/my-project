
function TwoFA() {
}

(function($) {
	
	$("body").ready(function() {

		Menu.displayButtons("#menu_dashbaord");
		Menu.enableSettings($("#_uid").val());

		TwoFA.enableGetAccess();
		
	});

	TwoFA.enableGetAccess = function() {
		$('a.tip').cluetip({splitTitle: '|'});
		$("#2fa_form").validate({
			rules: {
				country_input: {required:true},
				phone_input: {required:true, minlength:6,maxlength:15, number:true}
			},
			errorLabelContainer: '#2fa_form_error_container ul',
			wrapper: "li",
			submitHandler: function(form) {
				$("#2fa_submit_container").hide();
				$("#2fa_wait_container").show();
				admin.sendPinCode($("#country_input").val(),$("#phone_input").val(), function(data) {
					if (data==0) {
						$("#verify_container").show();
						$("#2fa_form_error_container").html('');
						TwoFA.enableVerify();
					} else if (data==2) {
						$("#2fa_form_error_container").html('This mobile number is already associated with an existing user.');

					} else {
						$("#2fa_form_error_container").html('We are not able to send a text message to your number, make sure to enter a valid mobile number.');
					}
					$("#2fa_submit_container").show();
					$("#2fa_wait_container").hide();
				});
    			return false;
			}
        });		
	};
	
	TwoFA.enableVerify = function() {
		$("#verify_form").validate({
			rules: {
				pin_input: {required:true, minlength:4,maxlength:4, number:true}
			},
			errorLabelContainer: '#verify_form_error_container ul',
			wrapper: "li",
			submitHandler: function(form) {
				$("#verify_submit_container").hide();
				$("#verify_wait_container").show();
				admin.checkPinCode($("#pin_input").val(), function(data) {
					if (data==0) {
						$("#verify_form_error_container").html('');
						window.location.reload();
					} else if (data==2) {
						window.location.reload();
					} else {
						$("#verify_form_error_container").html('We are not able to verify your access code.');
						$("#verify_submit_container").show();
						$("#verify_wait_container").hide();
					}
				});
    			return false;
			}
        });		

	};
	
	
	
})(jQuery);	
  