
function Notifications() {
}

(function($) {
	
	$("body").ready(function() {
		
		Menu.displayButtons(null);


		Menu.enableSettings($("#_uid").val());

			
		$("#notifications_form").validate({
			errorElement: "p",
			wrapper: "span",	
			errorPlacement: function(error, element) {
				error.addClass("error");
				element.after(error);
			},
			rules: {
				minBalance: {number:true,min:function() { return $("#creditLimit").val();},max:10000},
				financeEmail: {maxlength:150},
				pricingEmail: {maxlength:150},
				opsEmail: {maxlength:150}
			}
	      });
 			
		
	});


	
})(jQuery);	
  