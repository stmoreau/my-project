function TopUpBank() {
}

(function($) {

    $("body").ready(function() {
        Menu.displayButtons("#menu_topup");
        Menu.enableSettings($("#_uid").val());

        TopUpBank.customValidator();
        TopUpBank.bindEvents();
        TopUpBank.enableInvoiceRequest();
        
        var companyContry = $("#CompanyCountry").text();
        // ! $("#country").val() -- do not alter country if user had selected a different country and validation failure (of other fields) meant form redisplayed
        if (companyContry && ! $("#country").val()) {
        	if (console.log) console.log("Set country to "+ companyContry);
        	$("#country").val(companyContry);
    	}
    });

    TopUpBank.customValidator = function(){
        $.formUtils.addValidator({
                name : 'zipPostcode_custom',
                validatorFunction : function(value, $el, config, language, $form)
                {
                    if (value === '') {
                        $el.attr('data-validation-error-msg', config.validate.zipPostcode['error-msg-empty']);
                    }else if (value.length > 9){
                        $el.attr('data-validation-error-msg', config.validate.zipPostcode['error-msg-length']);
                    }else if (/^[a-zA-Z0-9_ -]+$/i.test(value) !== true){
                        $el.attr('data-validation-error-msg', config.validate.zipPostcode['error-msg-valid']);
                    }else{
                        return true;
                    }
                }
        });
    };

    TopUpBank.enableInvoiceRequest = function(){
        var rules = {
            form: '#invoiceRequestForm',
            validate: {
                'name': {
                    validation: 'required',
                    'error-msg': 'Please enter your full name'
                },
                'address1': {
                    validation: 'required',
                    'error-msg': 'Please enter your street address'
                },
                'city': {
                    validation: 'required',
                    'error-msg': 'Please enter your city'
                },
                'country': {
                    validation: 'required',
                   'error-msg': 'Please choose your country'
                },
                'zipPostcode': {
                     validation: 'zipPostcode_custom',
                     'error-msg-empty': 'Please enter your zip or postcode',
                     'error-msg-valid': 'Please enter a valid zip or postal code',
                     'error-msg-length': 'Please do not enter more then 9 characters'
                },
                'financeEmail': {
                    validation: 'email',
                    'error-msg': 'Please enter your email'
                },
                'amount': {
                    validation: 'number',
                   'allowing': 'range[200;100000]',
                   'error-msg': 'Please enter a number greater than or equal to 200'
                }
            }, 
            onSuccess: function(e) {
            	$("#invoice_add_submit_container").hide();
				$("#invoice_add_wait_container").show();
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });
    };

    TopUpBank.bindEvents = function(){
        $('form input').bind('validation', function(evt, isValid) { 
            if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide fa-check-circle').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });
    };
	
})(jQuery);