
function BuyNumbers() {
}

(function($) {

	$("body").ready(function() {
		BuyNumbers.enableNumberSearch();
		BuyNumbers.enableNumberSearchPagination();
		BuyNumbers.enableNumberBuy();
		BuyNumbers.showBuyConfirmationPopup();
	});

	BuyNumbers.enableNumberSearch = function() {
		$("#search_form").validate({
			rules: {
				search_country: {required:true},
				search_input: {number:true,minlength:2,maxlength:4}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			messages: {
				search_country: "Please choose a country.",
				search_input: {
					number: 'Please enter a valid number.',
					maxlength: 'Please enter maximum 4 digits.'
				}
			},
			submitHandler: function(form) {
				BuyNumbers.performNumberSearch($("#search_country").val(),$("#search_input").val(),$("#search_features").val(),$("#search_type").val(),1);
				return false;
			}
		});
	};

	BuyNumbers.performNumberSearch = function(country, pattern, features,type, index) {
		$("#search_submit_container").hide();
		$("#search_wait_container").show();
		$("#search_result_container").html('');
		view.getView('/widget/search/numbers/'+country+'?p='+pattern+'&i='+index+'&features='+features+'&type='+type, function(data) {
			if (data!=null && data!='void') {
				$("#search_result_container").html(data);
				$('#tableNumbers').removeClass('collapse');
				BuyNumbers.enableNumberSearchPagination();
			} else {
				$("#search_result_container").html('');
				alert("Your session has expired, please sign-in again!");
			}
			$("#search_submit_container").show();
			$("#search_wait_container").hide();
		});
	};

	BuyNumbers.enableNumberSearchPagination = function() {
		$(".number_search_pagination_link").each(function(i,o) { 
			$(this).click(function() {
				var index = $(this).attr('rel');
				view.getView('/widget/search/numbers/'+$("#search_country").val()+'?p='+$("#search_input").val()+'&i='+index+'&features='+$("#search_features").val()+'&type='+$("#search_type").val(), function(data) {
					if (data!=null && data!='void') {
						$("#search_result_container").html(data);
						$('#tableNumbers').removeClass('collapse');
						BuyNumbers.enableNumberSearchPagination();
					} else {
						$("#search_result_container").html('');
						alert("Your session has expired, please sign-in again!");
					}
				});
				return false;
			});
		});
	};

	BuyNumbers.enableNumberBuy = function() {
		$(document).on("click", ".number_buy_link", function(event){
			var splitRes = $(this).attr('rel').toString().split(":");
			var co = splitRes[0];
			var msisdn = splitRes[1];

			$('#search_form_error_container').hide();
			admin.buyNumber(co, msisdn,
					{ callback: function(ret) {
						if (ret==0) {
							BuyNumbers.performNumberSearch($("#search_country").val(),$("#search_input").val(),$("#search_features").val(),$("#search_type").val(),1);
							$('#confirmationModal').modal('hide');
							$('body').removeClass('modal-open');
							$('.modal-backdrop').remove();
						} else {
							if (ret==27) {
								$('#numbers_confirmation_error_container').html('<p class="text-danger pad-top"><i class="fa fa-exclamation-triangle"></i> Your account does not have enough funds to complete the purchase. Please do a top-up.</p>');
								$('.number_buy_link').attr('disabled', 'disabled');
							} else {
								$('#numbers_confirmation_error_container').html('<p class="text-danger pad-top"><i class="fa fa-exclamation-triangle"></i> Internal processing error!</p>');
								$('.number_buy_link').attr('disabled', 'disabled');
							}
						}
					},
					errorHandler: function(errorString, exception) {
						$('#search_form_error_container ul').html('<li>'+errorString+'</li>').show();
					}
					}
			);
		});
	};

	BuyNumbers.showBuyConfirmationPopup = function() {
		$(document).on("click", ".buyNumberModal", function(event){
			var msisdn = $(this).attr('rel');

			view.getView('/widget/show_buy_popup?co='+$("#search_country").val()+'&msisdn='+msisdn, function(data) {
				if (data) {
					$('#confirmationModal').html(data);
					$('#confirmationModal').modal('show');
				}
			});
			return false;
		});
	}

})(jQuery);

