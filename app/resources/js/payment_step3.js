function PaymentStep3() {
}

(function($) {

        $("body").ready(function() {
                Menu.displayButtons("#menu_topup");
                Menu.enableSettings($("#_uid").val());
                
                PaymentStep3.injectPaymentGatewayToken();
        });

        
        PaymentStep3.injectPaymentGatewayToken = function() {
        	var context = $("#_webapp_path").val();
            var dropin = $("#dropin");
            
            if (dropin.length) {                
				$.get(context + "/private/braintree", function( data ) {
						braintree.setup(
							data, 
							'dropin', {container: 'dropin'}
						);
				});
				
            } else if (window.console && window.console.log) {
            	console.log("div not found: likely caused by payment restrictions on this account");
            }	
    	};
        
})(jQuery);