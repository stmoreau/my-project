
function Login() {
}

(function($) {

	$("body").ready(function() {
		Login.customValidator();
		Login.validateInit();
		Login.bindEvents();
	});

    Login.customValidator = function(){

        $.formUtils.addValidator(
        {
            name : 'username_custom',
            validatorFunction : function(value, $el, config, language, $form) 
            {
            	if (value === ''){
            		$el.attr('data-validation-error-msg', config.validate.username['form-error']);
	            }else{
	                return true;
	            }
            }
       });
        
       $.formUtils.addValidator(
       {
            name : 'password_custom',
            validatorFunction : function(value, $el, config, language, $form) 
            {
            	if (value === ''){
            		$el.attr('data-validation-error-msg', config.validate.password['form-error']);
	            }else{
	                return true;
	            }
            }
       });
    };

    Login.validateInit = function(){
        var rules = {
            form: '.form-validate',
            validate: {
                'username': {
                    required:true,
                    email:true,
                    validation: 'username_custom',
                    'error-msg': 'Provide your email address'
                },
                'password': {
                    required:true,
                    validation: 'password_custom',
                    'error-msg': 'Provide your password'
                }
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });

    };

    Login.bindEvents = function(){
    	$('form input').bind('validation', function(evt, isValid) { 
        	if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });
    };

    Login.emailInfoShow = function(){
        $('[name="website"]').on('focus', function(){
            $('.website-info').removeClass('hide');
        });
    };

    Login.passwordInfoShow = function(){
        $('[name="website"]').on('focus', function(){
            $('.website-info').removeClass('hide');
        });
    };

})(jQuery);
