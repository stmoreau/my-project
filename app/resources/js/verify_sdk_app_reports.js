
function SDKAppAnalytics() {
}

(function($) {

	$("body").ready(function() {

		$("#select_date").change(function(){
			SDKAppAnalytics.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val(),$("#verify_sdk_appId").val());
		});

		if ( $('#select_account').is('*') ) {
			$("#select_account").change(function(){
				SDKAppAnalytics.reloadDates($("#select_type").val(),$("#select_account").val());
			});
		}

		//Cross-Browser solution Form Input Date Type
		function inputDateFix() {
			webshims.setOptions('waitReady', false);
			webshim.setOptions("forms-ext", {
				replaceUI: "auto",
				types: "date",
				date: {
					startView: 2,
					openOnMouseFocus: true,
					classes: "show-week"
				},
				number: {
					calculateWidth: false
				},
				range: {
					classes: "show-activevaluetooltip"
				}
			});
			webshims.polyfill('forms forms-ext');
			$.webshims.formcfg = {
					en: {
						dFormat: '-',
						dateSigns: '-',
						patterns: {
							d: "dd-mm-yy"
						}
					}
			};
		}
		inputDateFix();

	});

	SDKAppAnalytics.reloadDates = function (type,account,appId) {
		view.getView('/widget/verify/sdk/get_dates?t='+type+'&a='+account+"&appId="+appId, function(data) {
			$("#select_date").html(data);
			$("#reports_view_container").html('');
			SDKAppAnalytics.reloadView($("#select_type").val(),$("#select_date").val(),$("#select_account").val(),$("#verify_sdk_appId").val());
		});
	}

	SDKAppAnalytics.reloadView = function (type,date,account,appId) {
		$("#reports_view_container").html('');
		if (account!=null && account.length>0) {
			view.getView('/widget/verify/sdk/reports/view/'+date+'?t='+type+'&a='+account+"&appId="+appId, function(data) {
				$("#reports_view_container").html(data);
				SDKAppAnalytics.enableSorting();
			});
		} else {
			view.getView('/widget/verify/sdk/reports/view/'+date+'?t='+type+"&appId="+appId, function(data) {
				$("#reports_view_container").html(data);
				SDKAppAnalytics.enableSorting();
			});
		}
	}

	SDKAppAnalytics.enableSorting = function () {
		$('#report_table').tablesorter();
		var pageRow;

		$("#report_table").bind("sortStart", function () {
			// remove the row with the class nosort
			pageRow = $(this).children("tbody").children("tr.nosort");
			$(this).children("tbody").children("tr.nosort").remove();
		});

		$("#report_table").bind("sortEnd", function () {
			// add the removed row
			$(this).children("tbody").append(pageRow);
		});
	}

})(jQuery);

