
function PasswordReset() {
}

(function($) {

	$("body").ready(function() {

		PasswordReset.customValidator();
		PasswordReset.validateInit();
		PasswordReset.bindEvents();
		
	});

	PasswordReset.customValidator = function(){
        $.formUtils.addValidator(
        {
              name : 'password_custom',
              validatorFunction : function(value, $el, config, language, $form) 
              {
	                if (value === '') {
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-empty']);
	                    
	                }else if (value.length < 8 || value.length > 23 ){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-length']);
	                    
	                }else if (/[A-Z]/.test(value) !== true){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-capital']);
	                    
	                }else if (/\d/.test(value) !== true){
	                    $el.attr('data-validation-error-msg', config.validate.password['error-msg-digit']);
	                    
	                }else{                    
	                    return true;
	                }
              }
        });
	};
	        
	PasswordReset.validateInit = function(){
        var rules = {
            form: '.form-validate',
            validate: {
                'password': {
                    validation: 'password_custom',
                    'error-msg': 'default',
                    'error-msg-empty': 'Provide a password of at least 8 characters, including at least one capital letter and at least one number.',
                    'error-msg-length': 'Provide a password of between 8 and 23 characters',
                    'error-msg-capital': 'Include at least one capital letter in your password',
                    'error-msg-digit': 'Include at least one number in your password'
                },
                'confirmPassword': {
                    validation: 'password_custom',
                    'error-msg': 'default',
                    'error-msg-empty': 'Provide a password of at least 8 characters, including at least one capital letter and at least one number.',
                    'error-msg-length': 'Provide a password of between 8 and 23 characters',
                    'error-msg-capital': 'Include at least one capital letter in your password',
                    'error-msg-digit': 'Include at least one number in your password'
                }
                         
            }
        };

        $.validate({
            modules : 'jsconf',
            onModulesLoaded : function() {
                $.setupValidation(rules);
            }
        });
    };

    PasswordReset.bindEvents = function(){
    	$('form input').bind('validation', function(evt, isValid) { 
        	if(!isValid){
                $(evt.target).next('.icon-display').removeClass('hide').addClass('fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            }else{
                $(evt.target).next('.icon-display').removeClass('hide fa-exclamation-triangle').closest('.row').find('.question-help').hide();
            } 
        });

    };

})(jQuery);
