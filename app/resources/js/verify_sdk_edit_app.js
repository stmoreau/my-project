
function EndUserSDKAddApp() {
}

(function($) {

	$("body").ready(function() {

		jQuery.validator.addMethod("app_name_check", function(value, element) {
			var pattern = /^[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*$/;
			return pattern.test(value);
		},"Name should include no more than four digits");

		EndUserSDKAddApp.enableEditActions();
		EndUserSDKAddApp.enableAppDelete();
		EndUserSDKAddApp.showDeleteConfirmationPopup();


		$('.sdk_reset_link').each(function(i,o) {
			$(this).click(function() {
				var id = $(this).attr('rel');
				admin.resetSdkApplication(id,function(ret) {
					if (ret!= '') {
						location.reload();
					} else alert('Internal processing error!');
				});
			});
		});

	});


	EndUserSDKAddApp.enableAppDelete = function() {
		$(document).on("click", "#sdk_delete_confirm", function(event){
			var appId = $(this).attr('rel');
			admin.removeSdkApplication(appId,function(ret) {
				if (ret==true) {
					window.location.href = "/verify/sdk/your-apps";
				} else alert('Internal processing error!');
			});
		});
	};

	EndUserSDKAddApp.showDeleteConfirmationPopup = function() {
		$(document).on("click", "#sdk_delete_link", function(event){
			var appId = $(this).attr('rel');

			view.getView('/widget/delete_popup?appId='+appId, function(data) {
				if (data) {
					$('#sdkSettingsModal').html(data);
					$('#sdkSettingsModal').modal('show');
				}
			});
			return false;
		});
	}

	EndUserSDKAddApp.enableEditActions = function() {

		$('#sdk_scope_phoneNumber_edit').attr('disabled', true);
		$('.sdk_reset_link').click(function() {
			var id = $("#sdk_id_edit").val();
			admin.resetSdkApplication(id,function(ret) {
				if (ret!= '') {
					$("#sdk_shared_secret_edit").val(ret);
				} else alert('Internal processing error!');
			});
		});

		$("#custom_idle_edit").hide();
		var idleValue = $( "#sdk_idle_edit" ).val();
		if(idleValue === '-1'){
			$("#custom_idle_edit").show();
		}

		$( "#sdk_idle_edit" ).change(function(){
			var selectValue = $( "#sdk_idle_edit" ).val();
			if(selectValue === '-1'){
				$("#custom_idle_edit").show();
			}else{
				$("#custom_idle_edit").hide();
			}

		});

		if($( "#sdk_push_notification_edit" ).is(':checked')){
			$("#dynamic_fields_sdk_edit").show();
		}else{
			$("#dynamic_fields_sdk_edit").hide();
		}

		$( "#sdk_push_notification_edit" ).change(function(){
			var isPushEnabled = $( "#sdk_push_notification_edit" ).is(':checked');
			if(isPushEnabled){
				$("#dynamic_fields_sdk_edit").show();
			}else{
				$("#dynamic_fields_sdk_edit").hide();
			}
		});


		$("#sdk_edit_form").validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			rules: {
				pushSecret:{required:'#sdk_push_notification_edit:checked',minlength:1},
				pushIconURL:{required:'#sdk_push_notification_edit:checked',minlength:1},
				name: {app_name_check:true,required:true,maxlength:18},
				length: {required: true},
				idle: {required:true},
				customIdle: {required:true,number:true,min:1},
				tokenExpirationTime: {number:true,min:1,max:900000}//15minutes = 900000 milliseconds
			},
			submitHandler: function(form) {
				var idleValue = $("#sdk_idle_edit").val();
				var idleTimeUnit = 'day';
				if(idleValue == '-1'){
					idleValue = $("#sdk_custom_idle_edit").val();
				}else if(idleValue == '1'){
					idleTimeUnit = 'millisecond';
				}
				admin.editSdkApplication($("#sdk_id_edit").val(),
						$("#sdk_name_edit").val(),
						$("#sdk_pin_edit").val(),
						$("#sdk_scope_deviceIP_edit").is(':checked'),
						$("#sdk_scope_appID_edit").is(':checked'),
						$("#sdk_push_notification_edit").is(':checked'),
						$("#sdk_pushSecret_edit").val(),
						$("#sdk_push_iconURL_edit").val(),
						$("#sdk_push_silent_edit").is(':checked'),
						$("#sdk_push_subscription_edit").is(':checked'),
						idleValue,idleTimeUnit, function(ret) {
					if (ret!=null) {
						window.location.href = "/verify/sdk/your-apps";
					}
				});
				return false;
			}
		});
	};

})(jQuery);

