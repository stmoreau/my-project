
function Apps() {
}

(function($) {

	$("body").ready(function() {

		Apps.enableRevoke();

		Apps.enableAppsEdit();

		Apps.enableAppCreate();
		Apps.enableAppEditForm();
	});

	Apps.enableRevoke = function() {
		$('.revoke_link').each(function(i,o) {
			$(this).click(function() {
				admin.revokeApp($(this).attr('rel'),function(ret) {
					if (ret==true) {
						Apps.reloadApps();
					} else alert('Internal processing error!');
				});
			});
		});
	};

	Apps.reloadApps = function() {
		view.getView('/widget/apps', function(data) {
			$("#apps_list_container").html(data);
			Apps.enableRevoke();
		});
	};

	//Apps dev
	Apps.enableAppsEdit = function() {

		$('#app_dev_new_link').click(function() {
			view.getView('/widget/app_create', function(data) {
				$('#addAppModal').html(data);
				$('#addAppModal').modal('show');
				Apps.enableAppCreate();
			});
		});

		$('.app_edit_link').each(function(i,o) {
			$(this).click(function() {
				var v = $(this).attr('rel').split('##');
				view.getView('/widget/app/edit?a='+v[1]+'&k='+v[0], function(data) {
					$('#editAppModal').html(data);
					$('#editAppModal').modal('show');

					Apps.enableAppEditForm();
					$('#edit_app_cancel_link').click(function() {
						Apps.reloadAppsList();
					});
				});
			});
		});	
		$('.app_disable_link').each(function(i,o) {
			$(this).click(function() {
				var v = $(this).attr('rel').split('##');
				admin.disableApp(v[1],v[0],function(ret) {
					if (ret==true) {
						Apps.reloadAppsList();
					} else alert('Internal processing error!');
				});
			});
		});
		$('.app_enable_link').each(function(i,o) {
			$(this).click(function() {
				var v = $(this).attr('rel').split('##');
				admin.enableApp(v[1],v[0],function(ret) {
					if (ret==true) {
						Apps.reloadAppsList();
					} else alert('Internal processing error!');
				});
			});
		});
	};

	Apps.enableAppEditForm = function() {

		$("#app_edit_form").validate({
			rules: {
				app_name_edit: {required:true,minlength:3,maxlength:25},
				app_desc_edit: {maxlength:100},
				app_secret_edit: {required:true,minlength:30,maxlength:50},
				app_url_edit: {url:true}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			messages: {
				app_name_edit: {
					required: 'Please provide an app name.',
					minlength: 'Please enter minimum 3 characters.',
					maxlength: 'Please enter maximum 25 characters.'
				},
				app_desc_add: 'Please enter maximum 100 characters.',
				app_secret_edit: {
					required: 'Please provide a secret for the app.',
					minlength: 'Please enter minimum 30 characters',
					maxlength: 'Please enter maximum 50 characters.'
				}
			},
			submitHandler: function(form) {
				admin.editApp($("#_pk_account_id").val(),$("#_pk_app_id").val(),$("#app_name_edit").val(),$("#app_desc_edit").val(),$("#app_secret_edit").val(),$("#app_url_edit").val(), function(ret) {
					if (ret==true) {
						$('#editAppModal').modal('hide');
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
						Apps.reloadAppsList();
						Apps.reloadApps();
					}
				});
				return false;
			}
		});

	};

	Apps.reloadAppsList = function() {
		view.getView('/widget/apps_dev', function(data) {
			$("#apps_dev_list_container").html(data);
			Apps.enableAppsEdit();
		});
	};

	Apps.enableAppCreate =  function() {
		$("#app_add_form").validate({
			rules: {
				app_name_add: {required:true,minlength:3,maxlength:25},
				app_desc_add: {maxlength:100},
				app_url_add: {url:true}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			messages: {
				app_name_add: {
					required: 'Please provide an app name.',
					minlength: 'Please enter minimum 3 characters.',
					maxlength: 'Please enter maximum 25 characters.'
				},
				app_desc_add: 'Please enter maximum 100 characters.'
			},
			submitHandler: function(form) {
				admin.addApp($("#app_name_add").val(),$("#app_desc_add").val(),$("#app_url_add").val(), function(ret) {
					if (ret==true) {
						$('#addAppModal').modal('hide');
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
						Apps.reloadAppsList();
					}
				});
				return false;
			}
		});
	};

})(jQuery);

