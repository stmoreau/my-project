
function Reports() {
}

(function($) {

	$("body").ready(function() {

		var plot1;

		$("#select_period").change(function(){
			Reports.reload($("#select_period").val(),$("#select_type").val(),$("#select_account").val());
		});

		$("#select_type").change(function(){
			Reports.reload($("#select_period").val(),$("#select_type").val(),$("#select_account").val());
		});

		if ( $('#select_account').is('*') ) { 
			$("#select_account").change(function(){
				Reports.reload($("#select_period").val(),$("#select_type").val(),$("#select_account").val());
			});
		}

		Reports.loadChart($("#_json_data").val());

	});

	Reports.reload = function(day,type,account) {
		if (account!=null && account.length>0) {
			view.getView('/widget/messaging/view/'+day+"?t="+type+'&a='+account, function(data) {
				$("#reports_view_container").html(data);
				Reports.loadChart($("#_json_data").val());
			});

		} else {
			view.getView('/widget/messaging/view/'+day+"?t="+type, function(data) {
				$("#reports_view_container").html(data);
				Reports.loadChart($("#_json_data").val());
			});
		}
	};

	Reports.loadChart = function(data) {
		$(window).bind('resize', function(event, ui) {
			if (typeof plot1 != 'undefined') {
				plot1.replot( { resetAxes: true } );
			}
		});
		if(data) {
			var chart=jQuery.parseJSON(data);

			if (chart!=null && chart!='') {
				plot1 = $.jqplot('chartdiv', [chart], {
					//Options
					axesDefaults: {
						tickOptions: {
							showMark: false
						}
					},
					axes: {
						xaxis: {
							renderer:$.jqplot.DateAxisRenderer,
							tickRenderer:$.jqplot.CanvasAxisTickRenderer,
							tickOptions: {
								angle: -90,
								fontFamily: "Arial",
								fontSize: "8pt",
								formatString: "%d %b %Y",
								tickInterval: "1 day"
							}
						},
						yaxis: {
							min: 0,
							tickOptions:{formatString:'%d'}
						}
					},
					seriesDefaults: {
						color: "#35b30e",
						shadow: false,
						markerOptions: {
							shadow: false
						}
					},
					grid: {
						gridLineColor: "#e8e8e8",
						background: "#ffffff",
						borderColour: "#e8e8e8",
						borderWidth: 0,
						shadow: false
					},
					highlighter: {
						show: true,
						sizeAdjust: 7,
						showTooltip: true,
						tooltipLocation: "n",
						fadeTooltip: true,
						tooltipFadeSpeed: "fast",
						tooltipOffset: 3,
						tooltipAxes: "y",
						useAxesFormatters: true
					}

				});
			}
		}
	};

})(jQuery);
