
function SDKUserActivity() {
}

(function($) {

	$("body").ready(function() {
		SDKUserActivity.initValidation();
		SDKUserActivity.enableSearch();
		SDKUserActivity.enablePagination();

		//Cross-Browser solution Form Input Date Type
		function inputDateFix() {
			webshims.setOptions('waitReady', false);
			webshim.setOptions("forms-ext", {
				replaceUI: "auto",
				types: "date",
				date: {
					startView: 2,
					openOnMouseFocus: true,
					classes: "show-week"
				},
				number: {
					calculateWidth: false
				},
				range: {
					classes: "show-activevaluetooltip"
				}
			});
			webshims.polyfill('forms forms-ext');
			$.webshims.formcfg = {
					en: {
						dFormat: '-',
						dateSigns: '-',
						patterns: {
							d: "dd-mm-yy"
						}
					}
			};
		}
		inputDateFix();
	});


	SDKUserActivity.initValidation = function() {
		$.validator.addMethod("regex", function (value, element, regexp) {
			var re = new RegExp(regexp);
			return re.test(value);
		}, '');

		$.validator.addMethod('startDateRequired', function (value, element) {
			return $("#sdk_user_activity_start_date").val() !== "";
		}, 'Please choose a start date.');

		$.validator.addMethod('endDateRequired', function (value, element) {
			return $("#sdk_user_activity_end_date").val() !== "";
		}, 'Please choose an end date.');
	};


	SDKUserActivity.enableSearch = function() {
		$("#sdkUserActivityForm").validate({
			ignore: [], // http://stackoverflow.com/a/8565769
			rules: {
				sdk_user_activity_phone: {number:true, maxlength:16,required: true, minlength:6},
				start_date_validate:     {startDateRequired: true},
				end_date_validate:       {endDateRequired: true}
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				if (element.attr("name") == "sdk_user_activity_phone") {
					error.addClass("help-block form-error signup-nojs-error sdk-user-activity-phone-error");
				} else if (element.attr("name") == "start_date_validate") {
					error.addClass("help-block form-error signup-nojs-error sdk-user-activity-start-date-error");
				} else if (element.attr("name") == "end_date_validate") {
					error.addClass("help-block form-error signup-nojs-error sdk-user-activity-end-date-error");
				}
				error.appendTo(element.parent("div"));
			},
			submitHandler: function(form) {
				SDKUserActivity.performCdrsSearch($('#sdk_user_activity_start_date').val(),
												  $('#sdk_user_activity_end_date').val(),
												  $('#sdk_user_activity_phone').val(),
												  $('#sdk_user_activity_status').val(),
												  $('#sdk_user_activity_osFamily').val(),
												  $('#sdk_user_activity_app_name').val(),
												  1);
				return false;
			}
		});
	};


	SDKUserActivity.performCdrsSearch = function(start, end, msisdn, userStatus, osFamily, appId, index) {
		$("#sdk_user_activity_submit_container").hide();
		$("#sdk_user_activity_wait_container").show();
		view.getView('/widget/verify/sdk/user_activity/view?p='+index+'&start='+start+'&end='+end+'&msisdn='+msisdn
															+'&status='+userStatus+'&platform='+osFamily+'&appId='+appId, function(data) {
			if (data!=null && data!='void') {
				$("#verify_sdk_user_activity_reports").html(data);
				SDKUserActivity.enableDownload();
				SDKUserActivity.reloadDownloadParameters($("#sdk_user_activity_start_date").val(), $("#sdk_user_activity_end_date").val(), $("#sdk_user_activity_phone").val(),
														 $("#sdk_user_activity_status").val(), $("#sdk_user_activity_osFamily").val(), $("#sdk_user_activity_app_name").val());
			} else {
				$("#verify_sdk_user_activity_reports").html('');
				alert("Your session has expired, please sign-in again!");
			}
			$("#sdk_user_activity_submit_container").show();
			$("#sdk_user_activity_wait_container").hide();
		});
	};


	SDKUserActivity.enablePagination = function() {
		$(document).on("click", ".sdk_user_activity_pagination_link", function(event){
			var index = $(this).attr('rel');
			view.getView('/widget/verify/sdk/user_activity/view?p='+index+'&start='+$('#sdk_user_activity_start_date').val()+'&end='+$('#sdk_user_activity_end_date').val()
																	+'&msisdn='+$('#sdk_user_activity_phone').val()+'&status='+$('#sdk_user_activity_status').val()
																	+'&platform='+$('#sdk_user_activity_osFamily').val()+'&appId='+$('#sdk_user_activity_app_name').val(), function(data) {
				if (data!=null && data!='void') {
					$("#verify_sdk_user_activity_reports").html(data);
					SDKUserActivity.enableDownload();
					SDKUserActivity.reloadDownloadParameters($("#sdk_user_activity_start_date").val(), $("#sdk_user_activity_end_date").val(), $("#sdk_user_activity_phone").val(),
																 $("#sdk_user_activity_status").val(), $("#sdk_user_activity_osFamily").val(), $("#sdk_user_activity_app_name").val());
				} else {
					$("#verify_sdk_user_activity_reports").html('');
					alert("Your session has expired, please sign-in again!");
				}
				SDKUserActivity.enablePagination();
			});
			return false;
		});
	};

	SDKUserActivity.enableDownload = function() {
		if ( $('#sdk_user_activity_report_table').is('*') ) {
			$('#sdk_user_activity_download_link').attr("href",$('#download_link_prefix').val());
			$('#sdk_user_activity_download_link').show();
		}

		$("#sdk_user_activity_status").add("#sdk_user_activity_osFamily")
									  .add("#sdk_user_activity_app_name")
									  .add("#sdk_user_activity_start_date")
									  .add("#sdk_user_activity_end_date")
									  .add("#sdk_user_activity_phone")
									  .change(function() {
			SDKUserActivity.reloadDownloadParameters($("#sdk_user_activity_start_date").val(), $("#sdk_user_activity_end_date").val(), $("#sdk_user_activity_phone").val(),
													 $("#sdk_user_activity_status").val(), $("#sdk_user_activity_osFamily").val(), $("#sdk_user_activity_app_name").val());
		});
	};


	SDKUserActivity.reloadDownloadParameters = function (startDate, endDate, phoneNumber, userStatus, osFamily, applicationId) {
		var downloadLink = $('#sdk_user_activity_download_link');
		downloadLink.attr("href",$('#download_link_prefix').val() + '?start=' + startDate + '&end=' + endDate +"&msisdn=" + phoneNumber
								 + '&status=' + userStatus + '&platform=' + osFamily + '&appId=' + applicationId);
	};


})(jQuery);

