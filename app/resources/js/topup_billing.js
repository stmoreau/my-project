function TopUpBilling() {
}

(function($) {

    $("body").ready(function() {
    	
    	
        Menu.displayButtons("#menu_topup");
        Menu.enableSettings($("#_uid").val());

        if (typeof isPartialPage == 'undefined' || ! isPartialPage) {
        	TopUpBilling.topUpSubmit();
        } else {
        	if (console.log) console.log("isPartialPage ? "+ isPartialPage);
        }

        TopUpBilling.enablePaymentHistoryPagination();
        TopUpBilling.showDenomination();
        TopUpBilling.paymentCards();
        TopUpBilling.enableTopUpInvoice();
        TopUpBilling.switchInit();
        TopUpBilling.switchEvents();
        TopUpBilling.disableAutoReload();
    });

    TopUpBilling.reloadPaymentHistory = function(index) {
        view.getView('/widget/search/topup?p='+index, function(data) {
            $('#payment_history_container').html(data);
            TopUpBilling.enablePaymentHistoryPagination();
            TopUpBilling.enableTopUpInvoice();
        });
    };

    TopUpBilling.disableAutoReload = function(){
        $('#topup_disable_reload_link').click(function() {
            admin.disableAutoReload(function () {
                $('#autoreload_container').hide();
                $('#disable_autoreload_wait_container').show();
                window.location.reload();
            });
        });
    }

    TopUpBilling.enablePaymentHistoryPagination = function() {
        $(".topup_pagination_link").each(function(i,o) {
            $(this).click(function() {
                TopUpBilling.reloadPaymentHistory($(this).attr('rel'));
                return false;
            });
        });
    };

    TopUpBilling.paymentCards = function(){
        function formatCards (card) {
            if (!card.id || card.element.value == "-1") { return card.text; }
            var $card = $('<span><span class="cards ' + card.element.className.toLowerCase() + '"></span>' + card.text + '</span>');
            return $card;
        }
        $(".paymentCards").select2({
             templateSelection: formatCards,
             templateResult: formatCards,
             minimumResultsForSearch: Infinity
        });
    };
    
    TopUpBilling.enableTopUpInvoice = function() {
        $(".get_invoice_link").each(function(i,o) {
            $(this).click(function() {
            	var txn = $(this).attr('rel');
       	        var progressbar = $( "#progressbar" );
       	        var progressLabel = $( ".progress-label" );
       	        var timer;
       	        var invoiceDate = $("a[rel="+txn+"]").closest("tr").find("td:first").text();
            	$("#dialog-invoice p span").text(invoiceDate);
            	
        		// build the dialog and events
        		$("#dialog-invoice").dialog({
        	      modal: true,
        	      width: 400,
        	      open: function(event, ui) { 
        	    	  $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); 
        	    	  $(".ui-dialog-titlebar", ui.dialog | ui).hide();
        	    	  $(".ui-widget-overlay").css({
        		        			background:"rgb(0, 0, 0)",
        		        			opacity: ".50 !important",
        		        			filter: "Alpha(Opacity=50)"});
        	    	  
        	    	  
        	    	  // build the progress bar
  	        		  progressbar.progressbar({
  	        		      value: false,
  	        		      change: function() {
  	        		        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
  	        		      },
  	        		      complete: function() {
  	        		  		$("#dialog-invoice").dialog("close");
  	        		      }
  	        		  });

  	        		  function progress() {
  	        			var val = progressbar.progressbar( "value" ) || 0;
  	        			progressbar.progressbar( "value", val + 1 );
  	        			if ( val < 99 ) {
  	        			  timer = setTimeout( progress, 200 );
  	        			}
  	        		  }

  	        		  progress();
        	    	  
        	    	  // query invoice 
                      admin.generateTopUpInvoice(txn, function (ret) {
                    	  this.ret = ret;
                          if (ret==0) return; 
                          else if (ret==2)
                              alert('You need to fill out your company details in the profile section.');
                          else if (ret==3)
                              alert('The invoice has been sent already, if you want a copy contact our help desk.');
                          else if (ret==4)
                              alert('You do not have permissions for this request; please ask the user who made the payment to log-in and request the invoice.');
                          else if (ret==5)
                              alert('Please wait 20 seconds and try again.');                    
                          else
                              alert('We are not able to generate this invoice, please contact our help desk.');
          		  		  $("#dialog-invoice").dialog("close");
                      }.bind(this));
        	      },
        	      close: function(event, ui) {
        	    	  	clearTimeout(timer);
        	    	    progressbar.progressbar("destroy");
        	    	  	if(this.ret==0) {
                          alert('The invoice has been sent via email.');
                          $('a[rel='+txn+']').removeClass("env_ready").addClass("env_downloaded");
        	    	  	}
        	      }
        	    });    	

                return false;
            });
       });
    };

    
    TopUpBilling.topUpSubmit = function() {
         var merchantId;
         admin.getBraintreeMerchantId(function(r) { 
         	merchantId = r;
         });
         admin.getBraintreeEnvironment(function(ret) {
             if(ret=="SANDBOX"){ 
                 BraintreeData.setup(merchantId, 'braintreeData', BraintreeData.environments.sandbox);
             } else if(ret == "PRODUCTION"){
                 BraintreeData.setup(merchantId, 'braintreeData', BraintreeData.environments.production);
             }
         });
        $('#formTopUp').submit(function(e) {
            $("#topup_submit_container").hide();
            $("#topup_wait_container").show();
            $("#paymentInstrumentType").val($("#cardType").children(":selected").attr("class"));
        });
    };

    TopUpBilling.switchInit = function(){
        $(".switch").bootstrapSwitch({size: 'small'});
    };

    TopUpBilling.switchEvents = function(){
        $('[name="autoReload"]').on('switchChange.bootstrapSwitch', function(event, state) {
            if(state){
                
            }else{
            	var piId = $("#cardTypeAutoReload").children(":selected").attr("id");
                admin.disableAutoReload(piId);
            }
        });
    };

    TopUpBilling.showDenomination = function(){
        $("#cardType").change(function(){
            if($(this).val() == '-2')
                $("#denomination_container").hide();
            else $("#denomination_container").show();
        });
    };

})(jQuery);	
