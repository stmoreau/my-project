
function EndUserSDKAddApp() {
}

(function($) {

	$("body").ready(function() {

		jQuery.validator.addMethod("app_name_check", function(value, element) {
			var pattern = /^[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*\d?[a-zA-z-, _]*$/;
			return pattern.test(value);
		},"Name should include no more than four digits");

		EndUserSDKAddApp.enableAdd();
	});


	EndUserSDKAddApp.enableAdd = function() {
		$('#sdk_scope_phoneNumber_add').attr('disabled', true);
		$("#custom_idle_add").hide();
		$( "#sdk_idle_add" ).change(function(){
			var selectValue = $( "#sdk_idle_add" ).val();
			if(selectValue === '-1'){
				$("#custom_idle_add").show();
			}else{
				$("#custom_idle_add").hide();
			}
		});

		$("#dynamic_fields_sdk_add").hide();
		$( "#sdk_push_notification_add" ).change(function(){
			var isPushEnabled = $( "#sdk_push_notification_add" ).is(':checked');
			if(isPushEnabled){
				$("#dynamic_fields_sdk_add").show();
			}else{
				$("#dynamic_fields_sdk_add").hide();
			}
		});

		$("#sdk_add_form").validate({
			rules: {
				pushSecret:{required:'#sdk_push_notification_add:checked',minlength:1},
				pushIconURL:{required:'#sdk_push_notification_add:checked',minlength:1},
				name: {app_name_check:true,required:true,maxlength:18},
				length: {required: true},
				idle: {required:true},
				customIdle: {required:true,number:true,min:1},
				tokenExpirationTime: {number:true,min:1,max:900000}//15minutes = 900000 milliseconds
			},
			errorElement: "span",
			errorPlacement: function(error, element) {
				error.addClass("help-block form-error signup-nojs-error");
				error.insertAfter(element);
			},
			submitHandler: function(form) {
				$("#sdk_add_submit_container").hide();
				$("#sdk_add_wait_container").show();

				var idleValue = $("#sdk_idle_add").val();
				var idleTimeUnit = 'day';
				if(idleValue == '-1'){
					idleValue = $("#sdk_custom_idle_add").val();
				}else if(idleValue == '1'){
					idleTimeUnit = 'millisecond';
				}
				admin.addSdkApplication($("#sdk_name_add").val(),
						$("#sdk_key_add").val(),
						$("#sdk_mandatory_signature_add").is(':checked'),
						$("#sdk_pin_add").val(),
						$("#sdk_scope_deviceIP_add").is(':checked'),
						$("#sdk_scope_appID_add").is(':checked'),
						$("#sdk_push_notification_add").is(':checked'),
						$("#sdk_pushSecret_add").val(),
						$("#sdk_push_iconURL_add").val(),
						$("#sdk_push_silent_add").is(':checked'),
						$("#sdk_push_subscription_add").is(':checked'),
						idleValue,idleTimeUnit, function(ret) {
					if (ret!=null) {
						window.location.href = "/verify/sdk/your-apps";
					}
					$("#sdk_add_submit_container").show();
					$("#sdk_add_wait_container").hide();
				});
				return false;
			}
		});
	};

})(jQuery);

